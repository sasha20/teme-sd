package model;

import java.sql.Timestamp;
import java.util.Date; 


public class BillsModel {

	private int idbill;
	private String name;
	private double sum;
	private Date datedue;
	private int idclient;
	//private int accountid;
	
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public BillsModel(){}
	public BillsModel(String name, double sum ){
		this.name=name;
		this.sum=sum;
		//this.datedue = generateRandomDate();
		this.datedue = new Date();
	} 
	public Date generateRandomDate(){
		
		long rangebegin = Timestamp.valueOf("2015-04-03 00:00:00").getTime();
		long rangeend = Timestamp.valueOf("2015-05-04 00:58:00").getTime();
		long diff = rangeend - rangebegin + 1;
		Date dt = new Timestamp(rangebegin + (long)(Math.random() * diff));
	 return dt;
	}
	 
	public int getIdbill() {
		return idbill;
	}
	public void setIdbill(int idbill) {
		this.idbill = idbill;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}
	public Date getDatedue() {
		return datedue;
	}
	public void setDatedue(Date datedue) {
		this.datedue = datedue;
	} 
	public boolean equals(Object obj){
		if(obj ==null) return false;
		if(!this.getClass().equals(obj.getClass())) return false;
		
		BillsModel obj2 = (BillsModel) obj;
		if((this.idbill == obj2.getIdbill() && (this.name.equals(obj2.getName())))){
			return true;
		}
		return false;
	}
	public int hashCode(){
		int tmp= 0;
		tmp=  (idbill + name).hashCode();
		return tmp;
	}
	 
}
