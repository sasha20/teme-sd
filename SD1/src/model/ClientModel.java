package model;

import java.util.Set;

public class ClientModel {

	private String firstName;
	private String lastName;
	private int CNP;
	private String IDC;
	private String address;
	private int idclient;
	private Set clientac;
	private Set billac; 

	public ClientModel(){};
	public ClientModel(String firstName, String lastName, int CNP, String IDC, String address){
		this.address= address;
		this.lastName= lastName;
		this.firstName= firstName;
		this.IDC = IDC;
		this.CNP = CNP;
	}
	public Set getBillac() {
		return billac;
	}
	public void setBillac(Set billac) {
		this.billac = billac;
	}
	public Set getClientac() {
		return clientac;
	}
	public void setClientac(Set clientac) {
		this.clientac = clientac;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCNP() {
		return CNP;
	}
	public void setCNP(int cNP) {
		CNP = cNP;
	}
	public String getIDC() {
		return IDC;
	}
	public void setIDC(String iDC) {
		IDC = iDC;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public Set getClientAccount(){
		return clientac;
	}
	
	public void setClientAccount(Set clientac){
		this.clientac=clientac;
	}
 
}
 