package model; 
import java.util.Date;

public class ClientAccountModel {

	private int idclientaccount;
	private String type;
	private double amount;
	private Date date;
	private int idclient;
	private String accountid;

	public ClientAccountModel(){};
	
	public ClientAccountModel(String accountid,String type, double amount )
	{	this.accountid = accountid;
		this.type=type;
		this.amount=amount;
		this.date=new Date();
		
	}
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public int getIdclientaccount() {
		return idclientaccount;
	}
	public void setIdclientaccount(int idclientaccount) {
		this.idclientaccount = idclientaccount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public boolean equals(Object obj){
		if(obj ==null) return false;
		if(!this.getClass().equals(obj.getClass())) return false;
		
		ClientAccountModel obj2 = (ClientAccountModel) obj;
		if((this.idclientaccount == obj2.getIdclientaccount() && (this.type.equals(obj2.getType())))){
			return true;
		}
		return false;
	}
	
	public int hashCode(){
		int tmp= 0;
		tmp=  (idclientaccount + type).hashCode();
		return tmp;
	}
	
	
 
	
	
	
	
	
	
	
	
}
