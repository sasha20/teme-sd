package dbop;
 
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import model.AccountModel;
import model.BillsModel;
import model.ClientAccountModel;
import model.ClientModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
public class CRUD {

	private Utility ut = new Utility();
	private static SessionFactory factory;
	 
	/*
	 * 
	 * 
	 * Employee CRUD
	 ****************************************************************************************************************************************** 
	 * 
	 */
	/**
	 * Read Employees and send them to a Table
	 * @return
	 */
	public DefaultTableModel readEmployees( ){ 
		factory = ut.instantiate();
		
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("UserName");
			dtm.addColumn("Password");
			dtm.addColumn("Role");
			List employees = session.createQuery("FROM AccountModel").list(); 
			Iterator itr = employees.iterator();
			while(itr.hasNext()){
				
				AccountModel employee = (AccountModel) itr.next();
				dtm.addRow(new Object[]{
						employee.getUsername(), employee.getPassword(), employee.getRole()
				}); 
			 }
			tx.commit();
			return dtm;
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	}
	
	/**
	 * Find an Employee
	 * @param username
	 * @return
	 */
	public AccountModel find(String username){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		AccountModel acc = new AccountModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (AccountModel)session.createQuery("FROM AccountModel WHERE username= '"+ username   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{ 
				tx.commit();
				return acc; 
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	
	/**
	 * Create a new Employee
	 * @param username
	 * @param password
	 * @param role
	 * @return
	 */
	public boolean saveAccount(String username, String password, String role){
		factory = ut.instantiate();
		Integer employeeID = null;
		 AccountModel acc = new AccountModel(username,password,role);
		 AccountModel acc2 = new AccountModel();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			if((acc = (AccountModel)session.createQuery("FROM AccountModel WHERE username= '"+ username   +"'").uniqueResult())==null) 
			{	
			acc2.setUsername(username);
			acc2.setPassword(password);
			acc2.setRole(role);
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Employee
	 * @param EmployeeID
	 * @param username
	 * @param password
	 * @param role
	 */
	public void updateAccount(Integer EmployeeID,String username, String password, String role){
		factory = ut.instantiate();
		 Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			AccountModel account = (AccountModel)session.get(AccountModel.class, EmployeeID);
			account.setPassword(password);
			account.setRole(role);
			account.setUsername(username);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	/**
	 * Delete employee's Account
	 * @param EmployeeID
	 */
	public void deleteAccount(Integer EmployeeID){
		factory = ut.instantiate();  			 			 
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			AccountModel account = (AccountModel)session.get(AccountModel.class, EmployeeID);
			session.delete(account);
			tx.commit();
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * Client Info CRU
	 ****************************************************************************************************************************************** 
	 * 
	 */ 
	/**
	 * Read clients and send them to a table
	 * @return
	 */
	public DefaultTableModel readClient( ){ 
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("First Name");
			dtm.addColumn("Last Name");
			dtm.addColumn("CNP");
			dtm.addColumn("IDC");
			dtm.addColumn("Address");
			List clients = session.createQuery("FROM ClientModel").list(); 
			Iterator itr = clients.iterator();
			while(itr.hasNext()){
				
				ClientModel client = (ClientModel) itr.next();
				dtm.addRow(new Object[]{
						client.getFirstName(),client.getLastName(),client.getCNP(),client.getIDC(),client.getAddress()
				});
			 }
			tx.commit();
			return dtm;
			}	
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	} 
	/**
	 * Find Client
	 * @param CNP
	 * @return
	 */
	public ClientModel findClient(int CNP){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		ClientModel acc = new ClientModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (ClientModel)session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	/**
	 * Create Client
	 * @param firstName
	 * @param lastName
	 * @param CNP
	 * @param IDC
	 * @param address
	 * @return
	 */
	public boolean saveClient(String firstName, String lastName, int CNP, String IDC, String address){
		factory = ut.instantiate();
		Integer employeeID = null;
		 ClientModel acc = new ClientModel(firstName,lastName,CNP,IDC,address);
		 ClientModel acc2 = new ClientModel();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			if((acc = (ClientModel)session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").uniqueResult())==null) 
			{	
			acc2.setAddress(address);
			acc2.setCNP(CNP);
			acc2.setFirstName(firstName);
			acc2.setIDC(IDC);
			acc2.setLastName(lastName);
			
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Client
	 * @param EmployeeID
	 * @param firstName
	 * @param lastName
	 * @param CNP
	 * @param IDC
	 * @param address
	 */
	public void updateClient(Integer EmployeeID, String firstName, String lastName, int CNP, String IDC, String address){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientModel account = (ClientModel)session.get(ClientModel.class, EmployeeID);
		    account.setAddress(address);
		    account.setCNP(CNP);
		    account.setFirstName(firstName);
		    account.setIDC(IDC);
		    account.setLastName(lastName);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	
	
	
	
	/*
	 * 
	 * 
	 * Client's Account CRUD
	 ****************************************************************************************************************************************** 
	 * 
	 */ 
	 
	/**
	 * Find a client's Accounts
	 * @param idu
	 * @return
	 */
	public ClientAccountModel findClientAccount(String idu){
		factory = ut.instantiate();	
		Session session = factory.openSession();
		Transaction tx = null;
		ClientAccountModel acc = new ClientAccountModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (ClientAccountModel)session.createQuery("FROM ClientAccountModel WHERE accountid ='"+ idu   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	/**
	 * Create client account
	 * @param accountid
	 * @param type
	 * @param amount
	 * @param idcl
	 * @return
	 */
	public boolean saveClientAccount(String accountid,   String type,double amount ,int idcl ){
		factory = ut.instantiate();
		Integer employeeID = null;
		 ClientAccountModel acc = new ClientAccountModel(accountid, type,amount );
		 ClientAccountModel acc2  ;
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
 			if((acc = (ClientAccountModel)session.createQuery("FROM ClientAccountModel WHERE accountid ='" + accountid + "'").uniqueResult())==null) 
			{	
			acc2 = new ClientAccountModel(accountid,type,amount );
			acc2.setIdclient(idcl);
			
			
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Client's Account
	 * @param EmployeeID
	 * @param accountid
	 * @param type
	 * @param amount
	 */
	public void updateClientAccount(Integer EmployeeID, String accountid, String type, double amount){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientAccountModel account = (ClientAccountModel)session.get(ClientAccountModel.class, EmployeeID);
		    account.setType(type);
		    account.setAmount(amount);
		    account.setAccountid(accountid);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}	
	/**
	 * Delete Client's Account
	 * @param EmployeeID
	 */
	public void deleteClientAccount(Integer EmployeeID){
		factory = ut.instantiate();		 			 
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientAccountModel account = (ClientAccountModel)session.get(ClientAccountModel.class, EmployeeID);
			session.delete(account);
			tx.commit();
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	/**
	 * Read Client's Account
	 * @param CNP
	 * @return
	 */
	public DefaultTableModel readClientAccount(int CNP ){ 
		factory = ut.instantiate();Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			//dtm.addColumn("ID");
			dtm.addColumn("AccountID");
			dtm.addColumn("Type");
			dtm.addColumn("Amount");
			dtm.addColumn("Date");
			List clients = session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").list(); 
			Iterator itr = clients.iterator();
			while(itr.hasNext()){
				//while pentru Client
				ClientModel client = (ClientModel) itr.next();
				
				Set certificates = client.getClientAccount();
				for(Iterator iterator2 = certificates.iterator(); iterator2.hasNext();)
				{					//For pentru Cont
				
				ClientAccountModel  clientAc = (ClientAccountModel) iterator2.next();

		    	dtm.addRow(new Object[]{
					//	clientAc.getIdclientaccount(), 
						clientAc.getAccountid(),
						clientAc.getType(), 
						clientAc.getAmount(),
						clientAc.getDate()
				});
				}
			 }
			tx.commit();
			return dtm;}
	
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	 
		}
	
	
	
	
	
	
	/*
	 * 
	 * 
	 * Bills RD
	 ****************************************************************************************************************************************** 
	 * 
	 */  
	/**
	 * Find a client's Bill
	 * @param idu
	 * @return
	 */
	public BillsModel findBill(String idu){
		factory = ut.instantiate();	
		Session session = factory.openSession();
		Transaction tx = null;
		BillsModel acc = new BillsModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (BillsModel)session.createQuery("FROM BillsModel WHERE billname ='"+ idu   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	 	
		/**
		 * Delete Bill
		 * @param EmployeeID
		 */
		public void deleteBill(Integer EmployeeID){
			factory = ut.instantiate();			 			 
			Session session = factory.openSession();
			Transaction tx = null;
			try{ 
				tx = session.beginTransaction();
				BillsModel account = (BillsModel)session.get(BillsModel.class, EmployeeID);
				session.delete(account);
				tx.commit();
			}
			catch (HibernateException e) 
			{ if (tx!=null) 
				tx.rollback();
			e.printStackTrace(); }
			finally { 
				session.close(); 
			}
		}
		/**
		 * Read Bills
		 * @param CNP
		 * @return
		 */
		public DefaultTableModel readBills(int CNP ){ 
			factory = ut.instantiate();
			Session session = factory.openSession();
			Transaction tx = null;
			try{ 
				tx = session.beginTransaction(); 
				DefaultTableModel dtm = new DefaultTableModel();
				//dtm.addColumn("ID");
				dtm.addColumn("Name");
				dtm.addColumn("Amount");
				dtm.addColumn("DateDue");
		 		List clients = session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").list(); 
				Iterator itr = clients.iterator();
				while(itr.hasNext()){
					//while pentru Client
					ClientModel client = (ClientModel) itr.next();
					
					Set certificates = client.getBillac();
					for(Iterator iterator2 = certificates.iterator(); iterator2.hasNext();)
					{					//For pentru Cont
					
					BillsModel  clientAc = (BillsModel) iterator2.next();

			    	dtm.addRow(new Object[]{
						//	clientAc.getIdclientaccount(), 
							clientAc.getName(),
							clientAc.getSum(), 
							clientAc.getDatedue()
					});
					}
				 }
				tx.commit();
				return dtm;
				} 
			catch (HibernateException e) 
			{ if (tx!=null) 
				tx.rollback();
			e.printStackTrace(); }
			finally { session.close(); 
			}
			return null;
		 }
		 
}
	
	
 

