package dbop;

import javax.swing.table.DefaultTableModel;

import model.AccountModel;

public interface EmployeeDao {

	public DefaultTableModel readEmployees( );
	public AccountModel find(String username);
	public boolean saveAccount(String username, String password, String role);
	public void updateAccount(Integer EmployeeID,String username, String password, String role);
	public void deleteAccount(Integer EmployeeID);
}
