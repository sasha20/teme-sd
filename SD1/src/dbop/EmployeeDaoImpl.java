package dbop;

import java.util.Iterator;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import model.AccountModel;

public class EmployeeDaoImpl implements EmployeeDao{
	private Utility ut = new Utility();
	private static SessionFactory factory;
	
	/**
	 * Read Employees and send them to a Table
	 * @return
	 */
	public DefaultTableModel readEmployees( ){ 
		factory = ut.instantiate();
		
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("UserName");
			dtm.addColumn("Password");
			dtm.addColumn("Role");
			List employees = session.createQuery("FROM AccountModel").list(); 
			Iterator itr = employees.iterator();
			while(itr.hasNext()){
				
				AccountModel employee = (AccountModel) itr.next();
				dtm.addRow(new Object[]{
						employee.getUsername(), employee.getPassword(), employee.getRole()
				}); 
			 }
			tx.commit();
			return dtm;
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	}
	
	/**
	 * Find an Employee
	 * @param username
	 * @return
	 */
	public AccountModel find(String username){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		AccountModel acc = new AccountModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (AccountModel)session.createQuery("FROM AccountModel WHERE username= '"+ username   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{ 
				tx.commit();
				return acc; 
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	
	/**
	 * Create a new Employee
	 * @param username
	 * @param password
	 * @param role
	 * @return
	 */
	public boolean saveAccount(String username, String password, String role){
		factory = ut.instantiate();
		Integer employeeID = null;
		 AccountModel acc = new AccountModel(username,password,role);
		 AccountModel acc2 = new AccountModel();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			if((acc = (AccountModel)session.createQuery("FROM AccountModel WHERE username= '"+ username   +"'").uniqueResult())==null) 
			{	
			acc2.setUsername(username);
			acc2.setPassword(password);
			acc2.setRole(role);
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Employee
	 * @param EmployeeID
	 * @param username
	 * @param password
	 * @param role
	 */
	public void updateAccount(Integer EmployeeID,String username, String password, String role){
		factory = ut.instantiate();
		 Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			AccountModel account = (AccountModel)session.get(AccountModel.class, EmployeeID);
			account.setPassword(password);
			account.setRole(role);
			account.setUsername(username);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}

	@Override
	/**
	 * Delete employee's Account
	 * @param EmployeeID
	 */
	public void deleteAccount(Integer EmployeeID){
		factory = ut.instantiate();  			 			 
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			AccountModel account = (AccountModel)session.get(AccountModel.class, EmployeeID);
			session.delete(account);
			tx.commit();
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}

}
