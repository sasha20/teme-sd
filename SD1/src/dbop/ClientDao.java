package dbop;

import javax.swing.table.DefaultTableModel;

import model.ClientModel;

public interface ClientDao {

	public DefaultTableModel readClient( );
	public ClientModel findClient(int CNP);
	public boolean saveClient(String firstName, String lastName, int CNP, String IDC, String address);
	public void updateClient(Integer EmployeeID, String firstName, String lastName, int CNP, String IDC, String address);
	
}
