package dbop;

import javax.swing.table.DefaultTableModel;

import model.BillsModel;

public interface BillsDao {

	public BillsModel findBill(String idu);
	public void deleteBill(Integer EmployeeID);
	public DefaultTableModel readBills(int CNP );
}
