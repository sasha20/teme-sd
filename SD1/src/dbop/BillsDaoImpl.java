package dbop;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.table.DefaultTableModel;

import model.BillsModel;
import model.ClientModel;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BillsDaoImpl implements BillsDao {
	private Utility ut = new Utility();
	private static SessionFactory factory;
	
	/**
	 * Find a client's Bill
	 * @param idu
	 * @return
	 */
	public BillsModel findBill(String idu){
		factory = ut.instantiate();	
		Session session = factory.openSession();
		Transaction tx = null;
		BillsModel acc = new BillsModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (BillsModel)session.createQuery("FROM BillsModel WHERE billname ='"+ idu   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	 	
		/**
		 * Delete Bill
		 * @param EmployeeID
		 */
		public void deleteBill(Integer EmployeeID){
			factory = ut.instantiate();			 			 
			Session session = factory.openSession();
			Transaction tx = null;
			try{ 
				tx = session.beginTransaction();
				BillsModel account = (BillsModel)session.get(BillsModel.class, EmployeeID);
				session.delete(account);
				tx.commit();
			}
			catch (HibernateException e) 
			{ if (tx!=null) 
				tx.rollback();
			e.printStackTrace(); }
			finally { 
				session.close(); 
			}
		}
		/**
		 * Read Bills
		 * @param CNP
		 * @return
		 */
		public DefaultTableModel readBills(int CNP ){ 
			factory = ut.instantiate();
			Session session = factory.openSession();
			Transaction tx = null;
			try{ 
				tx = session.beginTransaction(); 
				DefaultTableModel dtm = new DefaultTableModel();
				//dtm.addColumn("ID");
				dtm.addColumn("Name");
				dtm.addColumn("Amount");
				dtm.addColumn("DateDue");
		 		List clients = session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").list(); 
				Iterator itr = clients.iterator();
				while(itr.hasNext()){
					//while pentru Client
					ClientModel client = (ClientModel) itr.next();
					
					Set certificates = client.getBillac();
					for(Iterator iterator2 = certificates.iterator(); iterator2.hasNext();)
					{					//For pentru Cont
					
					BillsModel  clientAc = (BillsModel) iterator2.next();

			    	dtm.addRow(new Object[]{
						//	clientAc.getIdclientaccount(), 
							clientAc.getName(),
							clientAc.getSum(), 
							clientAc.getDatedue()
					});
					}
				 }
				tx.commit();
				return dtm;
				} 
			catch (HibernateException e) 
			{ if (tx!=null) 
				tx.rollback();
			e.printStackTrace(); }
			finally { session.close(); 
			}
			return null;
		 }
		 
}
