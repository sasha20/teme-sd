package dbop;

import java.util.List;
import java.util.Iterator; 
import model.AccountModel;
import org.hibernate.HibernateException;
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class log  {
	private static SessionFactory factory;
	private Utility ut = new Utility();
 
	/**
	 * List all the accounts of the employees from the database
	 */
	public void listAccounts(){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			List accounts = session.createQuery("FROM AccountModel").list();
			for(Iterator iterator = accounts.iterator(); iterator.hasNext();){
			AccountModel account = (AccountModel) iterator.next();
			System.out.print("Username: " +account.getUsername());
			System.out.print("Password: " +account.getPassword());
			System.out.println("Role: "+ account.getRole());}
			tx.commit();
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			session.close();
		}
	}
	 
	/**
	 * Retrieve user from database
	 * @param username
	 * @param password
	 * @return
	 */
	public static AccountModel retrieveUser(String username,String password){
		try{ 
			factory = new Configuration().configure().buildSessionFactory(); }
		catch (Throwable ex) { System.err.println("Failed to create sessionFactory object." + ex);
		throw new ExceptionInInitializerError(ex); 
		}
		Session session = factory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			AccountModel account = (AccountModel)session.createQuery("FROM AccountModel WHERE username= '"+ username +"' AND password='"+password +"'").uniqueResult();
			tx.commit();
			return account;
		}catch(HibernateException e){
			if (tx!=null) tx.rollback();
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		
	}

		
	
	
	
}