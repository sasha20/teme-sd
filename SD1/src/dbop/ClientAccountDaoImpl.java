package dbop;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.table.DefaultTableModel;

import model.ClientAccountModel;
import model.ClientModel;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ClientAccountDaoImpl implements ClientAccountDao{

	private Utility ut = new Utility();
	private static SessionFactory factory;
	/**
	 * Find a client's Accounts
	 * @param idu
	 * @return
	 */
	public ClientAccountModel findClientAccount(String idu){
		factory = ut.instantiate();	
		Session session = factory.openSession();
		Transaction tx = null;
		ClientAccountModel acc = new ClientAccountModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (ClientAccountModel)session.createQuery("FROM ClientAccountModel WHERE accountid ='"+ idu   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	/**
	 * Create client account
	 * @param accountid
	 * @param type
	 * @param amount
	 * @param idcl
	 * @return
	 */
	public boolean saveClientAccount(String accountid,   String type,double amount ,int idcl ){
		factory = ut.instantiate();
		Integer employeeID = null;
		 ClientAccountModel acc = new ClientAccountModel(accountid, type,amount );
		 ClientAccountModel acc2  ;
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
 			if((acc = (ClientAccountModel)session.createQuery("FROM ClientAccountModel WHERE accountid ='" + accountid + "'").uniqueResult())==null) 
			{	
			acc2 = new ClientAccountModel(accountid,type,amount );
			acc2.setIdclient(idcl);
			
			
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Client's Account
	 * @param EmployeeID
	 * @param accountid
	 * @param type
	 * @param amount
	 */
	public void updateClientAccount(Integer EmployeeID, String accountid, String type, double amount){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientAccountModel account = (ClientAccountModel)session.get(ClientAccountModel.class, EmployeeID);
		    account.setType(type);
		    account.setAmount(amount);
		    account.setAccountid(accountid);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}	
	/**
	 * Delete Client's Account
	 * @param EmployeeID
	 */
	public void deleteClientAccount(Integer EmployeeID){
		factory = ut.instantiate();		 			 
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientAccountModel account = (ClientAccountModel)session.get(ClientAccountModel.class, EmployeeID);
			session.delete(account);
			tx.commit();
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	/**
	 * Read Client's Account
	 * @param CNP
	 * @return
	 */
	public DefaultTableModel readClientAccount(int CNP ){ 
		factory = ut.instantiate();Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			//dtm.addColumn("ID");
			dtm.addColumn("AccountID");
			dtm.addColumn("Type");
			dtm.addColumn("Amount");
			dtm.addColumn("Date");
			List clients = session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").list(); 
			Iterator itr = clients.iterator();
			while(itr.hasNext()){
				//while pentru Client
				ClientModel client = (ClientModel) itr.next();
				
				Set certificates = client.getClientAccount();
				for(Iterator iterator2 = certificates.iterator(); iterator2.hasNext();)
				{					//For pentru Cont
				
				ClientAccountModel  clientAc = (ClientAccountModel) iterator2.next();

		    	dtm.addRow(new Object[]{
					//	clientAc.getIdclientaccount(), 
						clientAc.getAccountid(),
						clientAc.getType(), 
						clientAc.getAmount(),
						clientAc.getDate()
				});
				}
			 }
			tx.commit();
			return dtm;}
	
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	 
		}
	
}
