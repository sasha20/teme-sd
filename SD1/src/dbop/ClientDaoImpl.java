package dbop;

import java.util.Iterator;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.ClientModel;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ClientDaoImpl implements ClientDao{
	
	private Utility ut = new Utility();
	private static SessionFactory factory;
	/**
	 * Read clients and send them to a table
	 * @return
	 */
	public DefaultTableModel readClient( ){ 
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction(); 
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("First Name");
			dtm.addColumn("Last Name");
			dtm.addColumn("CNP");
			dtm.addColumn("IDC");
			dtm.addColumn("Address");
			List clients = session.createQuery("FROM ClientModel").list(); 
			Iterator itr = clients.iterator();
			while(itr.hasNext()){
				
				ClientModel client = (ClientModel) itr.next();
				dtm.addRow(new Object[]{
						client.getFirstName(),client.getLastName(),client.getCNP(),client.getIDC(),client.getAddress()
				});
			 }
			tx.commit();
			return dtm;
			}	
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null;
	} 
	/**
	 * Find Client
	 * @param CNP
	 * @return
	 */
	public ClientModel findClient(int CNP){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		ClientModel acc = new ClientModel();
		try{ 
			tx = session.beginTransaction(); 
			
			if((acc = (ClientModel)session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").uniqueResult())==null) 
			{
				return null;
			}
			else{				
				tx.commit();
				return acc;			
			}
			}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { session.close(); 
		}
		return null; 
	}
	/**
	 * Create Client
	 * @param firstName
	 * @param lastName
	 * @param CNP
	 * @param IDC
	 * @param address
	 * @return
	 */
	public boolean saveClient(String firstName, String lastName, int CNP, String IDC, String address){
		factory = ut.instantiate();
		Integer employeeID = null;
		 ClientModel acc = new ClientModel(firstName,lastName,CNP,IDC,address);
		 ClientModel acc2 = new ClientModel();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			if((acc = (ClientModel)session.createQuery("FROM ClientModel WHERE CNP= '"+ CNP   +"'").uniqueResult())==null) 
			{	
			acc2.setAddress(address);
			acc2.setCNP(CNP);
			acc2.setFirstName(firstName);
			acc2.setIDC(IDC);
			acc2.setLastName(lastName);
			
			employeeID = (Integer) session.save(acc2);
			tx.commit();
			return true;
			}
			else		
			return false;
			
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
		return false;
	 }
	/**
	 * Update Client
	 * @param EmployeeID
	 * @param firstName
	 * @param lastName
	 * @param CNP
	 * @param IDC
	 * @param address
	 */
	public void updateClient(Integer EmployeeID, String firstName, String lastName, int CNP, String IDC, String address){
		factory = ut.instantiate();
		Session session = factory.openSession();
		Transaction tx = null;
		try{ 
			tx = session.beginTransaction();
			ClientModel account = (ClientModel)session.get(ClientModel.class, EmployeeID);
		    account.setAddress(address);
		    account.setCNP(CNP);
		    account.setFirstName(firstName);
		    account.setIDC(IDC);
		    account.setLastName(lastName);
			tx.commit();
		
		}
		catch (HibernateException e) 
		{ if (tx!=null) 
			tx.rollback();
		e.printStackTrace(); }
		finally { 
			session.close(); 
		}
	}
	
	
}
