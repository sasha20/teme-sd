package dbop;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Utility {
	private static SessionFactory factory;
	public SessionFactory instantiate()
	{
		try{ 	
		factory = new Configuration().configure().buildSessionFactory(); 
		return factory;
		}
	catch (Throwable ex) { System.err.println("Failed to create sessionFactory object." + ex);
	throw new ExceptionInInitializerError(ex); 
	}
	}
}
