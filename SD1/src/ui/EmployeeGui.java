package ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import model.BillsModel;
import model.ClientAccountModel;
import model.ClientModel;
import dbop.CRUD;

public class EmployeeGui extends JFrame {

	private JPanel contentPane;
	private JLabel lblWelcome;
	private JTable table;
	private JTextField textFieldCNP;
	private JTextField textFieldIDC;
	private JTextField textFieldAddress;
	private JTextField textFieldLastName;
	private JTextField textFieldFirstName;
	private JTable table_1;
	private JTextField textFieldAmount;
	private JTextField textFieldTransferTo;
	private JTextField textFieldTransferAmount;
	private CRUD a;
	private CRUD b;
	private CRUD c;
	private DefaultTableModel dtm;
	private DefaultTableModel dtm2;
	private DefaultTableModel dtm3;
	private int numar;
	private int numar2;
	private int numar3;
	private String numar4;
	private JTextField textFieldAccountId;
	private JComboBox<String> comboBox;
	private JTable table_3;
	private BillsModel bill;
	private ClientAccountModel clacc;
	/**
	 * Launch the application.
	 */
public static void main(String[] args) {
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmployeeGui frame = new EmployeeGui();
					frame.setVisible(true);
					 
				
					 
						 
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				 
			}
		});
		 
	}

	public void welcome(String username ){
		 this.lblWelcome.setText("Welcome "+username  );
	}
	
	public void FillData(){
		
		
		   a  = new CRUD();
		   dtm = a.readClient();
		   this.table.setModel(dtm);
		   
		}
	
	public void FillData2(){
		
		   b = new CRUD();
		   dtm2 =b.readClientAccount(numar2);
		   this.table_1.setModel(dtm2);
	}
	
	public void FillData3(){
			c = new CRUD();
			dtm3 = c.readBills(numar2);
			this.table_3.setModel(dtm3);
		
	}
	
	/**
	 * Create the frame.
	 */
	public EmployeeGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 585, 424);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 153, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblWelcome = new JLabel("Welcome,");
		lblWelcome.setBounds(10, 11, 172, 27);
		contentPane.add(lblWelcome);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 49, 549, 326);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 102, 102));
		tabbedPane.addTab("Client Info", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 524, 134);
		panel.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			
				int index=  table.getSelectedRow();
				int  CNP = (int) table.getValueAt(index, 2);
				 ClientModel acc= a.findClient(CNP);
				 textFieldAddress.setText(acc.getAddress());
				 textFieldFirstName.setText(acc.getFirstName());
				 textFieldCNP.setText(Integer.toString(acc.getCNP()));
				 textFieldLastName.setText(acc.getLastName());
				 textFieldIDC.setText(acc.getIDC());
				 numar =acc.getIdclient();
			     numar2 = acc.getCNP();
				 FillData2();
				 FillData3();
			}
		});
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(204, 153, 153));
		panel_2.setBorder(new TitledBorder(null, "Actions", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(402, 156, 132, 134);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String firstName = textFieldFirstName.getText();
					String lastName = textFieldLastName.getText();
					String IDC = textFieldIDC.getText();
					String address = textFieldAddress.getText();
					int CNP =  Integer.parseInt(textFieldCNP.getText());
					
					if(a.saveClient(firstName, lastName, CNP, IDC, address)==true){
						//a.saveAccount(username, password, role);
						JOptionPane.showMessageDialog(null, "Added a new Client!");
					FillData();
					}
					else{
				      JOptionPane.showMessageDialog(null, "Client Already Exists!");
					}
				}catch(Exception z){
					JOptionPane.showMessageDialog(null, z.getMessage());
				}
				
			}
		});
		btnCreate.setBounds(25, 28, 89, 37);
		panel_2.add(btnCreate);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//TO DO UPDATE CODE
				try{
					String firstName = textFieldFirstName.getText();
					String lastName = textFieldLastName.getText();
					String IDC = textFieldIDC.getText();
					String address = textFieldAddress.getText();
					int CNP =  Integer.parseInt(textFieldCNP.getText());
					
					a.updateClient(numar, firstName,lastName,CNP,IDC,address);
					 JOptionPane.showMessageDialog(null, "Successfully Updated Info!");
					FillData();
					}
					catch(Exception ex){JOptionPane.showMessageDialog(null,"Please select a field in the table");
					}
			}
		});
		btnUpdate.setBounds(25, 86, 89, 37);
		panel_2.add(btnUpdate);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 204, 153));
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(10, 156, 382, 131);
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		textFieldCNP = new JTextField();
		textFieldCNP.setColumns(10);
		textFieldCNP.setBounds(286, 11, 86, 20);
		panel_3.add(textFieldCNP);
		
		JLabel lblCnp = new JLabel("CNP :");
		lblCnp.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblCnp.setBounds(213, 14, 63, 14);
		panel_3.add(lblCnp);
		
		textFieldIDC = new JTextField();
		textFieldIDC.setColumns(10);
		textFieldIDC.setBounds(286, 48, 86, 20);
		panel_3.add(textFieldIDC);
		
		JLabel lblIdc = new JLabel("IDC :");
		lblIdc.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblIdc.setBounds(213, 51, 63, 14);
		panel_3.add(lblIdc);
		
		textFieldAddress = new JTextField();
		textFieldAddress.setColumns(10);
		textFieldAddress.setBounds(116, 92, 256, 20);
		panel_3.add(textFieldAddress);
		
		JLabel lblAddress = new JLabel("Address :");
		lblAddress.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblAddress.setBounds(10, 95, 96, 14);
		panel_3.add(lblAddress);
		
		JLabel lblLastName = new JLabel("Last Name :");
		lblLastName.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblLastName.setBounds(10, 51, 97, 14);
		panel_3.add(lblLastName);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setColumns(10);
		textFieldLastName.setBounds(117, 48, 86, 20);
		panel_3.add(textFieldLastName);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setColumns(10);
		textFieldFirstName.setBounds(117, 11, 86, 20);
		panel_3.add(textFieldFirstName);
		
		JLabel lblFirstName = new JLabel("First Name :");
		lblFirstName.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblFirstName.setBounds(10, 14, 97, 14);
		panel_3.add(lblFirstName);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 102, 102));
		tabbedPane.addTab("Client Accounts", null, panel_1, null);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 524, 95);
		panel_1.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			try{	
				int index=  table_1.getSelectedRow();
				String  idaccount = (String) table_1.getValueAt(index, 0);
				
				 ClientAccountModel clac= b.findClientAccount(idaccount);
							 
				 //textFieldIdentNr.setText(Integer.toString(clac.getIdclientaccount()));
				 textFieldAccountId.setText(clac.getAccountid());
				 if(clac.getType().equals("spendings")){
					 comboBox.setSelectedItem("spendings");
				 }else if(clac.getType().equals("savings")){
					 comboBox.setSelectedItem("savings");
				 }
				
				 textFieldAmount.setText(Double.toString(clac.getAmount()));
			 
				 numar3 = clac.getIdclientaccount();
				 numar4 = clac.getAccountid();
				  System.out.println(numar3);
			}
			catch(Exception e){
				JOptionPane.showMessageDialog(null, "Please select a client from other tab!");
			}
				 
				
			}
		});
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		scrollPane_1.setViewportView(table_1);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(245, 222, 179));
		panel_4.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, SystemColor.infoText, new Color(0, 0, 0)));
		panel_4.setBounds(10, 117, 276, 170);
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblType = new JLabel("Type :");
		lblType.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblType.setBounds(10, 53, 99, 14);
		panel_4.add(lblType);
		
		JLabel lblAmount = new JLabel("Amount :");
		lblAmount.setFont(new Font("Perpetua Titling MT", Font.BOLD, 11));
		lblAmount.setBounds(10, 84, 99, 14);
		panel_4.add(lblAmount);
		
		textFieldAmount = new JTextField();
		textFieldAmount.setBounds(143, 80, 123, 20);
		panel_4.add(textFieldAmount);
		textFieldAmount.setColumns(10);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(new Color(245, 222, 179));
		panel_6.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_6.setBounds(10, 111, 256, 48);
		panel_4.add(panel_6);
		panel_6.setLayout(null);
		
		JButton btnCreate_1 = new JButton("Create");
		btnCreate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					String type =(String) comboBox.getSelectedItem();
					int idclientaccount = numar2;
					double amountofmoney = Double.parseDouble(textFieldAmount.getText());
					int idclient = numar;
					String accountId = textFieldAccountId.getText();
					
					if(b.saveClientAccount(accountId,  type,amountofmoney , numar)==true){
						//a.saveAccount(username, password, role);
						JOptionPane.showMessageDialog(null, "Added a new account to this client!");
					FillData();
					FillData2();
					}
					else{
				      JOptionPane.showMessageDialog(null, "Account ID Already Exists!");
					}
				}catch(Exception z){
					JOptionPane.showMessageDialog(null, z.getMessage());
				}
				
				
			}
		});
		btnCreate_1.setBounds(0, 0, 77, 48);
		panel_6.add(btnCreate_1);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			 	//update
				try{
					String type =(String) comboBox.getSelectedItem();
				//	int idclientaccount = numar2;
					double amountofmoney = Double.parseDouble(textFieldAmount.getText());
				//	int idclient = numar;
					String accountId = textFieldAccountId.getText();
					
					b.updateClientAccount(numar3, accountId, type, amountofmoney);
					 JOptionPane.showMessageDialog(null, "Successfully Updated Info!");
					FillData2();
					}
					catch(Exception ex){JOptionPane.showMessageDialog(null,"Please select a field in the table");
					}
				
				
			}
		});
		btnUpdate_1.setBounds(87, 0, 77, 48);
		panel_6.add(btnUpdate_1);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				  try{
				int result = JOptionPane.showConfirmDialog(null, "Are you sure?","Confirm", JOptionPane.YES_NO_OPTION);
				 
					 if(result==JOptionPane.YES_OPTION)
					 {
				
				a.deleteClientAccount(numar3);
				FillData2();
					 }
					 
					 
					 }
				 catch(Exception xxx){
					 JOptionPane.showMessageDialog(null, "Select a field in the table first.");
				 } 
			}
		});
		btnDelete.setBounds(174, 0, 82, 48);
		panel_6.add(btnDelete);
		
		JLabel lblAccountId = new JLabel("Account ID :");
		lblAccountId.setBounds(10, 11, 116, 31);
		panel_4.add(lblAccountId);
		
		textFieldAccountId = new JTextField();
		textFieldAccountId.setBounds(143, 16, 123, 20);
		panel_4.add(textFieldAccountId);
		textFieldAccountId.setColumns(10);
		
		 
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"spendings", "savings"}));
		comboBox.setBounds(143, 49, 123, 20);
		panel_4.add(comboBox);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(296, 113, 238, 174);
		panel_1.add(tabbedPane_1);
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(new Color(204, 204, 102));
		tabbedPane_1.addTab("Transfer", null, panel_8, null);
		panel_8.setLayout(null);
		
		JLabel lblTo = new JLabel("To Account ID :");
		lblTo.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblTo.setBounds(10, 25, 117, 16);
		panel_8.add(lblTo);
		
		JLabel lblAmount_2 = new JLabel("Amount :");
		lblAmount_2.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblAmount_2.setBounds(10, 84, 117, 14);
		panel_8.add(lblAmount_2);
		
		textFieldTransferTo = new JTextField();
		textFieldTransferTo.setBounds(137, 21, 86, 20);
		panel_8.add(textFieldTransferTo);
		textFieldTransferTo.setColumns(10);
		textFieldTransferTo.setText("RO");
		
		textFieldTransferAmount = new JTextField();
		textFieldTransferAmount.setBounds(137, 81, 86, 20);
		panel_8.add(textFieldTransferAmount);
		textFieldTransferAmount.setColumns(10);
		
		
		
		
		
		
		
	 
		JButton btnTransfer = new JButton("Transfer");
		btnTransfer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					String name = textFieldTransferTo.getText();
					double amt = Double.parseDouble(textFieldTransferAmount.getText());
					
					ClientAccountModel accDin  =a.findClientAccount(numar4);
					ClientAccountModel accCatre = b.findClientAccount(name); 
					
					int nru = accCatre.getIdclientaccount();
					
					if((accDin !=null) &&(accCatre !=null)){						
						
						if((accDin.getAmount()-amt) < 0)
						{
							JOptionPane.showMessageDialog(null, "You cannot transfer that much from " + accDin.getAccountid().toString());
					
						}
						else if(amt<50)  {
							JOptionPane.showMessageDialog(null, "You cannot transfer less than 50$ "  );
						}
						else if( textFieldTransferAmount.getText().equals("")==true ){
							JOptionPane.showMessageDialog(null, "Please insert an amount you want to transfer");
						}
						else if((amt > 500)){
							JOptionPane.showMessageDialog(null, "Amount to be transfer cannot exccel 500$");
						}
						else{
						accDin.setAmount(accDin.getAmount()-amt);
						accCatre.setAmount(accCatre.getAmount()+amt);
								
						
						a.updateClientAccount(numar3, accDin.getAccountid(), accDin.getType(), accDin.getAmount());
						a.updateClientAccount(nru, accCatre.getAccountid(), accCatre.getType(), accCatre.getAmount());
						JOptionPane.showMessageDialog(null, "Transfer Complete!");
						FillData2();
						}
						}					
				}
				catch(Exception e){
					JOptionPane.showMessageDialog(null, "Please select an account to transfer from!");
				}
				
			}
		});
		
		
		
		
		btnTransfer.setFont(new Font("SimSun", Font.BOLD, 11));
		btnTransfer.setBounds(69, 112, 105, 23);
		panel_8.add(btnTransfer);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(new Color(255, 222, 173));
		tabbedPane_1.addTab("Utility Bills", null, panel_5, null);
		panel_5.setLayout(null);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 11, 213, 73);
		panel_5.add(scrollPane_3);
		
		table_3 = new JTable();
		table_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				try{	
					int index=  table_1.getSelectedRow();
					String  idaccount = (String) table_1.getValueAt(index, 0);
					
					int index2 =table_3.getSelectedRow();
					String idbill = (String) table_3.getValueAt(index2, 0);
								
					   clacc= b.findClientAccount(idaccount);
					   bill = c.findBill(idbill);
										 
					  	System.out.println(clacc.getAmount()  +"   " + bill.getName());			 
				}
				catch(Exception e){
					JOptionPane.showMessageDialog(null, "Please select a bill and account in order to pay!");
				}
				
				///pt a-mi afisa Billele
				
				
			 
				
			}
		});
		table_3.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"New column", "New column", "New column"
			}
		));
		scrollPane_3.setViewportView(table_3);
		
		JButton btnPaySelected = new JButton("Pay Selected Bill");
		btnPaySelected.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnPaySelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					int nr= bill.getIdbill();
				
				 if(bill.getSum()>clacc.getAmount()){
					 JOptionPane.showMessageDialog(null, "Insufficient money on this account in order to pay the bill!");
				 }
				 else{
				 clacc.setAmount(clacc.getAmount()-bill.getSum());
				 b.updateClientAccount(numar3, clacc.getAccountid(), clacc.getType(), clacc.getAmount());
				 c.deleteBill(nr);
			 
				  
				 JOptionPane.showMessageDialog(null, "Successfully Payed the Bill!");
					 FillData2();
					 FillData3();
				 }
				}catch(Exception E){
					JOptionPane.showMessageDialog(null, "Please select a Bill & Account in order to pay!");
				}
			}
		});
		btnPaySelected.setBounds(64, 95, 114, 40);
		panel_5.add(btnPaySelected);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setFont(new Font("Comic Sans MS", Font.BOLD, 11));
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					LoginGui gg = new LoginGui();
					gg.setVisible(true);
					setVisible(false);						
				} catch (Exception e) {
					// TODO: handle exception
					JOptionPane.showMessageDialog(null, "Error Logging Out, Retry");
				}
			}
		});
		btnLogout.setBounds(473, 13, 86, 36);
		contentPane.add(btnLogout);
		
		JLabel lblEmployeeView = new JLabel("Employee View");
		lblEmployeeView.setFont(new Font("Perpetua Titling MT", Font.BOLD, 16));
		lblEmployeeView.setBounds(210, 11, 163, 38);
		contentPane.add(lblEmployeeView);
		
		
		FillData();
		
	}
}
