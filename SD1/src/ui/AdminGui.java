package ui; 
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import bussinessLogic.EmployeeServiceImpl;
import model.AccountModel; 
import dbop.CRUD;
 

public class AdminGui extends JFrame {

	private JPanel contentPane;
	private JTable jTableAccount;
	private JScrollPane s;
	private EmployeeServiceImpl a ;
	private DefaultTableModel dtm;
	private JLabel lblWelcome;
	private JLabel lblUsername;
	private JTextField textFieldUsername;
	private JLabel lblPassword;
	private JTextField textFieldPassword;
	private JLabel lblRole;
	private JTextField textFieldRole;
	private JButton btnDelete;
	private int numar;
	private JLabel lblAdminView;
 
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	
	public void FillData(){ 
		   a  = new EmployeeServiceImpl();
		   dtm = a.readEmployees();
		   this.jTableAccount.setModel(dtm);
			
		}
	public void welcome(AccountModel a ){
		 this.lblWelcome.setText("Welcome "+ a.getUsername()  );
	}
	public AdminGui() {
		setTitle("Admin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 533, 388);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 153, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(27, 48, 465, 127);
		contentPane.add(scrollPane);
		
		jTableAccount = new JTable();
		jTableAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try{
				 
						int index=  jTableAccount.getSelectedRow();
						String username = jTableAccount.getValueAt(index, 0).toString();
						 AccountModel acc= a.find(username);
						 textFieldUsername.setText(acc.getUsername());
						 textFieldPassword.setText(acc.getPassword());
						 textFieldRole.setText(acc.getRole());
					     numar =acc.getId();
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				
			}
		});
		jTableAccount.setBackground(Color.WHITE);
		scrollPane.setViewportView(jTableAccount);
		jTableAccount.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"New column", "New column", "New column"
			}
		));
		
		JPanel accountInfo = new JPanel();
		accountInfo.setBackground(new Color(173, 216, 230));
		accountInfo.setBorder(new TitledBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0))), "Account Details", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		accountInfo.setBounds(27, 186, 358, 153);
		contentPane.add(accountInfo);
		accountInfo.setLayout(null);
		
		lblUsername = new JLabel("Username :");
		lblUsername.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblUsername.setBounds(10, 28, 94, 26);
		accountInfo.add(lblUsername);
		
		textFieldUsername = new JTextField();
		textFieldUsername.setBounds(133, 31, 157, 20);
		accountInfo.add(textFieldUsername);
		textFieldUsername.setColumns(10);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblPassword.setBounds(10, 65, 94, 26);
		accountInfo.add(lblPassword);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(133, 68, 157, 20);
		accountInfo.add(textFieldPassword);
		textFieldPassword.setColumns(10);
		
		lblRole = new JLabel("Role :");
		lblRole.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblRole.setBounds(10, 103, 94, 20);
		accountInfo.add(lblRole);
		
		textFieldRole = new JTextField();
		textFieldRole.setBounds(133, 103, 157, 20);
		accountInfo.add(textFieldRole);
		textFieldRole.setColumns(10);
		
		lblWelcome = new JLabel("Hi, ");
		lblWelcome.setBounds(27, 11, 163, 26);
		contentPane.add(lblWelcome);
		
		JButton btnSave = new JButton("Create");
		btnSave.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try{
					String username = textFieldUsername.getText();
					String password = textFieldPassword.getText();
					String role = textFieldRole.getText();
					
					if(a.saveAccount(username, password, role)==true){
						//a.saveAccount(username, password, role);
						JOptionPane.showMessageDialog(null, "Added new account!");
					FillData();
					}
					else{
				      JOptionPane.showMessageDialog(null, "User already exisists!");
					}
				}catch(Exception z){
					JOptionPane.showMessageDialog(null, z.getMessage());
				}
				
			
			}
		});
		btnSave.setBounds(403, 206, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnBack = new JButton("Logout");
		btnBack.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					 LoginGui gg = new LoginGui();
					 gg.setVisible(true);
					 setVisible(false);
					
					
				}
				catch(Exception x)
				{JOptionPane.showMessageDialog(null, x.getMessage());
					
				}
				
			}
		});
		btnBack.setBounds(403, 14, 89, 23);
		contentPane.add(btnBack);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//codu pt update
				
				try{
					
					 
				 a.updateAccount(numar, textFieldUsername.getText(), textFieldPassword.getText(), textFieldRole.getText());
				 JOptionPane.showMessageDialog(null, "Successfully Updated Info!");
				FillData();
				}
				catch(Exception ex){JOptionPane.showMessageDialog(null,"Please select a field in the table");
				}
			}
		});
		btnUpdate.setBounds(403, 253, 89, 23);
		contentPane.add(btnUpdate);
		
		
		
		/**
		 * Delete Employee
		 */
		btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 
				//codu pt delete
				 try{
					 
					 int result = JOptionPane.showConfirmDialog(null, "Are you sure?","Confirm", JOptionPane.YES_NO_OPTION);
				 
					 if(result==JOptionPane.YES_OPTION)
					 { 
				a.deleteAccount(numar);
				FillData();
					 } 
					 }
				 catch(Exception xxx){
					 JOptionPane.showMessageDialog(null, "Select a field in the table first.");
				 }
			}
		});
		btnDelete.setBounds(403, 301, 89, 23);
		contentPane.add(btnDelete);
		
		lblAdminView = new JLabel("Admin View");
		lblAdminView.setFont(new Font("Perpetua Titling MT", Font.BOLD, 17));
		lblAdminView.setBounds(184, 17, 137, 20);
		contentPane.add(lblAdminView);

		FillData();
	}
	

	public static void main(String[] args) { 
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminGui frame = new AdminGui();
					frame.setVisible(true); 
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		});
		 
	}
}
