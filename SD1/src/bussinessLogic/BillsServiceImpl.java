package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import model.BillsModel;
import dbop.BillsDaoImpl;

public class BillsServiceImpl implements BillsService {

	@Override
	public BillsModel findBill(String idu) {
		BillsDaoImpl dao = new BillsDaoImpl();
		return dao.findBill(idu);
	}

	@Override
	public void deleteBill(Integer EmployeeID) {
		BillsDaoImpl dao = new BillsDaoImpl();
		dao.deleteBill(EmployeeID);
		
	}

	@Override
	public DefaultTableModel readBills(int CNP) {
		BillsDaoImpl dao = new BillsDaoImpl();
		return dao.readBills(CNP);
	}

}
