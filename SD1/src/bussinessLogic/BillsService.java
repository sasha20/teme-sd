package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import model.BillsModel;

public interface BillsService {
	public BillsModel findBill(String idu);
	public void deleteBill(Integer EmployeeID);
	public DefaultTableModel readBills(int CNP );
}
