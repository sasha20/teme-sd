package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import model.ClientAccountModel;

public interface ClientAccountService {
	public ClientAccountModel findClientAccount(String idu);
	public boolean saveClientAccount(String accountid,   String type,double amount ,int idcl );
	public void updateClientAccount(Integer EmployeeID, String accountid, String type, double amount);
	public void deleteClientAccount(Integer EmployeeID);
	public DefaultTableModel readClientAccount(int CNP );
}
