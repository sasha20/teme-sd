package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import dbop.ClientAccountDaoImpl;
import model.ClientAccountModel;

public class ClientAccountServiceImpl implements ClientAccountService{

	@Override
	public ClientAccountModel findClientAccount(String idu) {
		ClientAccountDaoImpl dao = new ClientAccountDaoImpl();
		return dao.findClientAccount(idu);
	}

	@Override
	public boolean saveClientAccount(String accountid, String type,
			double amount, int idcl) {
		// TODO Auto-generated method stub
		ClientAccountDaoImpl dao = new ClientAccountDaoImpl();
		return dao.saveClientAccount(accountid, type, amount, idcl);
	}

	@Override
	public void updateClientAccount(Integer EmployeeID, String accountid,
			String type, double amount) {
		// TODO Auto-generated method stub
		ClientAccountDaoImpl dao = new ClientAccountDaoImpl();
		dao.updateClientAccount(EmployeeID, accountid, type, amount);
	}

	@Override
	public void deleteClientAccount(Integer EmployeeID) {
		// TODO Auto-generated method stub
		ClientAccountDaoImpl dao = new ClientAccountDaoImpl();
		dao.deleteClientAccount(EmployeeID);
	}

	@Override
	public DefaultTableModel readClientAccount(int CNP) {
		// TODO Auto-generated method stub
		ClientAccountDaoImpl dao = new ClientAccountDaoImpl();
		return dao.readClientAccount(CNP);
	}

}
