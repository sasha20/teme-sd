package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import model.ClientModel;
import dbop.ClientDaoImpl;

public class ClientServiceImpl implements ClientService{

	@Override
	public DefaultTableModel readClient() {
		ClientDaoImpl dao = new ClientDaoImpl();
		return dao.readClient();
	}

	@Override
	public ClientModel findClient(int CNP) {
		ClientDaoImpl dao = new ClientDaoImpl();
		return dao.findClient(CNP);
	}

	@Override
	public boolean saveClient(String firstName, String lastName, int CNP,
			String IDC, String address) {
		ClientDaoImpl dao = new ClientDaoImpl();
		return dao.saveClient(firstName, lastName, CNP, IDC, address);
	}

	@Override
	public void updateClient(Integer EmployeeID, String firstName,
			String lastName, int CNP, String IDC, String address) {
		// TODO Auto-generated method stub
		ClientDaoImpl dao = new ClientDaoImpl();
		dao.updateClient(EmployeeID, firstName, lastName, CNP, IDC, address);
	}

}
