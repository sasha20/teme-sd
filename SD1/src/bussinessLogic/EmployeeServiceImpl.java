package bussinessLogic;

import javax.swing.table.DefaultTableModel;

import dbop.EmployeeDaoImpl;
import model.AccountModel;

public class EmployeeServiceImpl implements EmployeeService{

	@Override
	public DefaultTableModel readEmployees() {
		EmployeeDaoImpl dao = new EmployeeDaoImpl();
		return dao.readEmployees();
	}

	@Override
	public AccountModel find(String username) {
		// TODO Auto-generated method stub
		EmployeeDaoImpl dao = new EmployeeDaoImpl();
		return dao.find(username);
	}

	@Override
	public boolean saveAccount(String username, String password, String role) {
		// TODO Auto-generated method stub
		EmployeeDaoImpl dao = new EmployeeDaoImpl();
		return dao.saveAccount(username, password, role);
	}

	@Override
	public void updateAccount(Integer EmployeeID, String username,
			String password, String role) {
		EmployeeDaoImpl dao = new EmployeeDaoImpl();
		dao.updateAccount(EmployeeID, username, password, role);
		// TODO Auto-generated method stub
		
	} 
	@Override
	public void deleteAccount(Integer EmployeeID) {
		// TODO Auto-generated method stub
		EmployeeDaoImpl dao = new EmployeeDaoImpl();
		dao.deleteAccount(EmployeeID);
	}

}
