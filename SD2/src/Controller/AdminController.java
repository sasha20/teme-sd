package Controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import Factory.Report;
import Factory.ReportFactory;
import Model.User;
import Model.UserList;
import Service.UserCrud;
import View.AdminView;

/**
 * Admin Controller for AdminGui
 * 
 * @author Sasha
 * 
 */
public class AdminController {
	private UserList theModel;
	private AdminView theView;
	private UserList userList;
	private ArrayList<User> list;
	private UserCrud userCrud;
	private DefaultTableModel dtm;

	private Report repo;

	public AdminController() {
	}

	public AdminController(UserList user, AdminView adminView)
			throws FileNotFoundException, JAXBException {
		this.theModel = user;
		this.theView = adminView;
		userCrud = new UserCrud();
		this.list = userCrud.readUsers();
		this.theView.setTable(tableUsers(list));
		// theView.setVisible(false);
		this.theView.addUpdateListener(new UpdateListener());
		this.theView.addMouseListener(new ClickListener());
		this.theView.addInsertListener(new InsertListener());
		this.theView.addDeleteListener(new DeleteListener());
		this.theView.addGenerateReport(new GenerateListener());
		this.theView.addOpenReport(new OpenListener());
	}

	/**
	 * Listener for Update button in AdminView
	 * 
	 * @author Sasha
	 * 
	 */
	class UpdateListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String username, password, email, usernameDinTabel;
			String[] stringu;
			try {
				ArrayList<User> listaNoua = new ArrayList<User>();
				UserCrud users = new UserCrud();
				AdminController acc = new AdminController();
				username = theView.getUsername();
				password = theView.getPassword();
				email = theView.getEmail();
				usernameDinTabel = theView.getIndexTabel();
				stringu = acc.getSearchedUser(usernameDinTabel);

				/*
				 * stringocea[0] = username; stringocea[1] = password;
				 * stringocea[2] = email; stringocea[3] = stringu[3];
				 * System.out.println("Viata:" + stringocea[0] + " "
				 * +stringocea[1] + " "+stringocea[2]+ " " +stringocea[3]);
				 */
				userCrud = new UserCrud();
				userCrud.updateUser(Integer.parseInt(stringu[3]), username,
						password, email);

				// repaint
				theView.setFields(username, password, email);
				listaNoua = users.readUsers();
				theView.setTable(tableUsers(listaNoua));
				JOptionPane.showMessageDialog(null, "Update Succeeded!");

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"Please Select a field first before updating");
			}

		}
	}

	/**
	 * Listener for delete button
	 * 
	 * @author Sasha
	 * 
	 */
	class DeleteListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String username, password, email;
			User us = new User();
			UserCrud users = new UserCrud();
			ArrayList<User> listaNoua = new ArrayList<User>();

			try {
				username = theView.getUsername();
				password = theView.getPassword();
				email = theView.getEmail();

				UserCrud newUser = new UserCrud();

				if (username.equals("") || password.equals("")
						|| email.equals("")) {
					JOptionPane
							.showMessageDialog(null,
									"Select a user from table that you want to delete!");
				} else {

					int result = JOptionPane.showConfirmDialog(null,
							"Are you sure?", "Confirm",
							JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {

						newUser.deleteUser(username);
						// repaint
						theView.setFields(username, password, email);
						listaNoua = users.readUsers();
						theView.setTable(tableUsers(listaNoua));
						JOptionPane
								.showMessageDialog(null, "Delete Succeeded!");
					}
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Something went wrong!");
			}
		}
	}

	/**
	 * Listener for Open button
	 * 
	 * @author Sasha
	 * 
	 */
	class OpenListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				Desktop.getDesktop().open(new File("./reports"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Listener for Generate report button
	 * 
	 * @author Sasha
	 * 
	 */
	class GenerateListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String combo;

			try {
				combo = theView.getCombo();
				ReportFactory report = new ReportFactory();
				if (combo.equals("TXT")) {
					repo = report.getReport("Txt");
					repo.generate();
					JOptionPane.showMessageDialog(null,
							"Generated a .txt file!");
				} else if (combo.equals("XML")) {
					repo = report.getReport("Xml");
					repo.generate();
					JOptionPane.showMessageDialog(null,
							"Generated a .xml file!");
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Something went wrong!");
			}
		}

	}

	/**
	 * Listener for Insert button
	 * 
	 * @author Sasha
	 * 
	 */
	class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String username, password, email;
			UserCrud us = new UserCrud();
			UserCrud users = new UserCrud();
			ArrayList<User> listaNoua = new ArrayList<User>();
			try {
				username = theView.getUsername();
				password = theView.getPassword();
				email = theView.getEmail();

				User us2 = us.searchUser(username);
				UserCrud newUser = new UserCrud();
				if (us2 != null) {
					// aci mai multe if-uri pt Email si username
					JOptionPane.showMessageDialog(null,
							"Deja exista un cont cu numele acesta!");
				} else {

					newUser.insertUser(username, password, email);
					// repaint
					theView.setFields(username, password, email);
					listaNoua = users.readUsers();
					theView.setTable(tableUsers(listaNoua));
					JOptionPane.showMessageDialog(null, "Insert Succeeded!");
				}

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Couldn't Insert!");
			}
		}

	}

	/**
	 * Listener for table clicks
	 * 
	 * @author Sasha
	 * 
	 */
	class ClickListener implements MouseListener {

		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			try {
				AdminController acc = new AdminController();
				String usernameDinTabel = theView.getIndexTabel();
				String[] stringu = acc.getSearchedUser(usernameDinTabel);

				// test
				System.out.println(stringu[3]);
				theView.setFields(stringu[0], stringu[1], stringu[2]);

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Ceva n-o mers bine");
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}
	}

	/**
	 * Set up a table model to send to the AdminView table for the users
	 * 
	 * @param list
	 * @return
	 */
	public DefaultTableModel tableUsers(ArrayList<User> list) {
		try {
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("UserName");
			dtm.addColumn("Password");
			dtm.addColumn("Email");
			for (User user : list) {
				dtm.addRow(new Object[] { user.getUsername(),
						user.getPassword(), user.getEmail() });
			}
			return dtm;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Place a User's information into an array of strings
	 * 
	 * @param username
	 * @return
	 * @throws FileNotFoundException
	 * @throws JAXBException
	 */
	public String[] getSearchedUser(String username)
			throws FileNotFoundException, JAXBException {
		UserCrud use = new UserCrud();
		User listaTemporara = use.searchUser(username);
		String[] stringu = { listaTemporara.getUsername(),
				listaTemporara.getPassword(), listaTemporara.getEmail(),
				Integer.toString(listaTemporara.getId()) };
		return stringu;
	}

}
