package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Model.Bookstore;
import Model.User;
import Model.UserList;
import Service.UserCrud;
import View.AdminView;
import View.LogView;
import View.UserView;

/**
 * Login controller to select between admin or regular and display corresponding
 * GUI
 * Used for LogView
 * @author Sasha
 * 
 */
public class LoginController {

	private UserCrud theModel;
	private LogView theLogView;
	private User a;
	private ActionListener actionListener;

	// model //view
	public LoginController(UserCrud user, LogView log) {
		this.theModel = user;
		this.theLogView = log;

		this.theLogView.addLoginListener(new LoginListener());
	}

	class LoginListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String username, password;
			try {
				username = theLogView.getUsername();
				password = theLogView.getPassword();
				if (username.equals("") || password.equals(""))
					JOptionPane.showMessageDialog(null, "Empty Credentials");
				else {
					a = theModel.unmarshall(username, password);  
					if (a == null) {
						JOptionPane.showMessageDialog(null, "User not found");
					} else if (a.getId() == 1) {
						AdminView theAdminView = new AdminView();
						UserList theAdminModel = new UserList();
						Bookstore theBookModel = new Bookstore();
					 
						AdminController theController = new AdminController(
								theAdminModel, theAdminView);
						BooksController theController2 = new BooksController(
								theBookModel, theAdminView);

						theAdminView.welcome(a.getUsername());
						theAdminView.setVisible(true);
						theLogView.setVisible(false);
					} else {
						UserView userView = 	 new UserView();
						Bookstore theBookModel = new Bookstore(); 
						UserController theController = new UserController(theBookModel, userView); 
						userView.welcome(a.getUsername());
						userView.setVisible(true);
						theLogView.setVisible(false);
					}
				}
			} catch (Exception E) {
				E.printStackTrace();
			}
		}
	}

}