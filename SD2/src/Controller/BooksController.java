package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import Model.Book;
import Model.Bookstore;
import Service.BookCrud;
import View.AdminView;

/**
 * Books controller for the Admin View
 * 
 * @author Sasha
 * 
 */
public class BooksController {

	private Bookstore theModel;
	private AdminView theView;
	private ArrayList<Book> list;
	private BookCrud crud;

	public BooksController(Bookstore bookModel, AdminView adminView)
			throws FileNotFoundException, JAXBException {
		this.theModel = bookModel;
		this.theView = adminView;
		this.crud = new BookCrud();
		this.list = crud.readBooks();
		this.theView.setTableBooks(tableBooks(list));
		this.theView.addMouseListenerBook(new ClickListener());
		this.theView.addUpdateListenerBook(new UpdateListener());
		this.theView.addInsertListenerBook(new InsertListener());
		this.theView.addDeleteListenerBook(new DeleteListener());
	}

	public BooksController() {
	}

	class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int quantity;
			double price;
			String title, author, genre, bookDinTabel;

			try {
				ArrayList<Book> listaNoua = new ArrayList<Book>();
				BookCrud us = new BookCrud();
				title = theView.getTitle();
				author = theView.getAuthor();
				genre = theView.getGenre();
				quantity = Integer.parseInt(theView.getQuantity());
				price = Double.parseDouble(theView.getPrice());

				Book us2 = us.searchTitle(title);
				if (us2 != null) {
					JOptionPane.showMessageDialog(null,
							"Deja exista o carte cu numele acesta!");
				} else if (quantity < 0) {
					JOptionPane.showMessageDialog(null,
							"Can't have negative quantity!");

				} else {
					crud.insertBook(title, author, genre, quantity, price);
					// repaint
					theView.setFieldsBook(title, author, genre, quantity, price);
					listaNoua = crud.readBooks();
					theView.setTableBooks(tableBooks(listaNoua));
					JOptionPane.showMessageDialog(null, "Insert Succeeded!");
				}

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Couldn't Insert!");
			}
		}
	}

	class UpdateListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String title, author, genre, bookDinTabel;
			String[] stringu;
			int quantity;
			double price;
			try {
				BooksController acc = new BooksController();
				ArrayList<Book> listaNoua = new ArrayList<Book>();
				title = theView.getTitle();
				author = theView.getAuthor();
				genre = theView.getGenre();
				quantity = Integer.parseInt(theView.getQuantity());
				price = Double.parseDouble(theView.getPrice());

				bookDinTabel = theView.getIndexTabelBooks();
				stringu = acc.getSearchedBook(bookDinTabel);
				crud = new BookCrud();
				crud.updateBooks(Integer.parseInt(stringu[5]), title, author,
						genre, quantity, price);

				// repaint
				theView.setFieldsBook(title, author, genre, quantity, price);
				listaNoua = crud.readBooks();
				theView.setTableBooks(tableBooks(listaNoua));
				JOptionPane.showMessageDialog(null, "Update Succeeded!");
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"Please Select a field first before updating");
			}
		}
	}

	class DeleteListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String title, author, genre, bookDinTabel;
			String[] stringu;
			int quantity;
			double price;
			try {
				BooksController acc = new BooksController();
				ArrayList<Book> listaNoua = new ArrayList<Book>();
				title = theView.getTitle();
				author = theView.getAuthor();
				genre = theView.getGenre();
				String quant = theView.getQuantity();
				quantity = Integer.parseInt(theView.getQuantity());
				price = Double.parseDouble(theView.getPrice());

				int result = JOptionPane.showConfirmDialog(null,
						"Are you sure?", "Confirm", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION) {

					crud.deleteBook(title);
					// repaint
					theView.setFieldsBook(title, author, genre, quantity, price);
					listaNoua = crud.readBooks();
					theView.setTableBooks(tableBooks(listaNoua));
					JOptionPane.showMessageDialog(null, "Delete Succeeded!");
				}

			} catch (Exception E) {
				JOptionPane.showMessageDialog(null,
						"Select a book from table that you want to delete!");
			}

		}

	}

	class ClickListener implements MouseListener {

		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			try {
				BooksController acc = new BooksController();
				String bookTitleDinTabel = theView.getIndexTabelBooks();
				String[] stringu = acc.getSearchedBook(bookTitleDinTabel);

				// test
				System.out.println(stringu[3]);
				theView.setFieldsBook(stringu[0], stringu[1], stringu[2],
						Integer.parseInt(stringu[3]),
						Double.parseDouble(stringu[4]));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Ceva n-o mers bine");
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}
	}

	public DefaultTableModel tableBooks(ArrayList<Book> list) {
		try {
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("Title");
			dtm.addColumn("Author");
			dtm.addColumn("Genre");
			dtm.addColumn("Quantity");
			dtm.addColumn("Price");

			for (Book book : list) {
				dtm.addRow(new Object[] { book.getTitle(), book.getAuthor(),
						book.getGenre(), book.getQuantity(), book.getPrice() });
			}
			return dtm;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getSearchedBook(String title) throws FileNotFoundException,
			JAXBException {
		// returneaza un user
		BookCrud book = new BookCrud();
		Book book2 = new Book();
		book2 = book.searchTitle(title);
		String[] stringu = { book2.getTitle(), book2.getAuthor(),
				book2.getGenre(), Integer.toString(book2.getQuantity()),
				Double.toString(book2.getPrice()),
				Integer.toString(book2.getId()) };
		// numar = listaTemporara.getId();
		return stringu;
	}
}