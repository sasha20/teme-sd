package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

import Model.Book;
import Model.Bookstore;
import Service.BookCrud;
import View.UserView;

/**
 * Controller for the Regular User  in UserView
 * @author Sasha
 *
 */
public class UserController {

	private Bookstore theModel;
	private UserView theView;
	private ArrayList<Book> list;
	private BookCrud crud;

	public UserController() {
	}

	public UserController(Bookstore bookModel, UserView userView)
			throws FileNotFoundException, JAXBException {
		this.theModel = bookModel;
		this.theView = userView;
		this.crud = new BookCrud();
		this.list = crud.readBooks();
		this.theView.setTableBooks(tableBooks(list));
		this.theView.addMouseListenerBook(new ClickListener());
		this.theView.addSearchListener(new SearchListener());
		this.theView.addSellListener(new SellListener());
	}

	public DefaultTableModel tableBooks(ArrayList<Book> list) {
		try {
			DefaultTableModel dtm = new DefaultTableModel();
			dtm.addColumn("Title");
			dtm.addColumn("Author");
			dtm.addColumn("Genre");
			dtm.addColumn("Quantity");
			dtm.addColumn("Price");

			for (Book book : list) {
				dtm.addRow(new Object[] { book.getTitle(), book.getAuthor(),
						book.getGenre(), book.getQuantity(), book.getPrice() });
			}
			return dtm;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	class SellListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String qua;
			int quantity = 0;
			String title;

			try {
				if (theView.getSell().equals("")) {
					JOptionPane.showMessageDialog(null,
							"The field is empty, enter a number plox!");
				} else {
					try {
						// aci ii sellu
						quantity = Integer.parseInt(theView.getSell());
						System.out.println(quantity);
						Book book = new Book();
						Book book2 = new Book();
						title = theView.getTitle();

						if (title.equals("")) {
							JOptionPane.showMessageDialog(null,
									"Select a book from the table!");
						} else {
							book = crud.searchTitle(title);
							if (book.getQuantity() == 0) {
								JOptionPane.showMessageDialog(null,
										"Out of stock!");
							} else if (book.getQuantity() < quantity) {
								JOptionPane
										.showMessageDialog(null,
												"Quantity of book must be greater than desired quantity!");
							}

							else { // ramura binelui suprem
								book = crud.searchTitle(title);
								ArrayList<Book> listaNoua = new ArrayList<Book>();
								crud.updateBookQuantity(book.getId(), quantity);
								book2 = crud.searchTitle(title);
								// repaint
								theView.setFieldsBook(book2.getTitle(),
										book2.getAuthor(), book2.getGenre(),
										book2.getQuantity(), book2.getPrice());
								listaNoua = crud.readBooks();
								theView.setTableBooks(tableBooks(listaNoua));
							}
						}
					} catch (NumberFormatException ex) {
						JOptionPane.showMessageDialog(null,
								"This is not a number!");
					}

				}

			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Couldn't sell!");
			}

			/*
			
			*/
		}
	}

	class SearchListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String search, combo;
			// Book book = new Book();
			ArrayList<Book> rez = new ArrayList<Book>();
			try {
				search = theView.getSearch();
				combo = theView.getCombo();

				if (search.equals("")) {
					JOptionPane.showMessageDialog(null,
							"Please enter something in the search field!");
				} else if (search.equals("*")) {
					rez = crud.readBooks();
					theView.setTableBooks(tableBooks(rez));
				}

				else {
					rez = crud.searchBook(search, combo);

					if (rez.size() == 0) {
						JOptionPane.showMessageDialog(null,
								"Couldn't find book!");
					} else {
						for (Book book : rez) {
							System.out.println("Found: " + book.getTitle()
									+ " " + book.getAuthor());
							// repaint table with found one(s)

							theView.setTableBooks(tableBooks(rez));

						}
					}
				}

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Couldn't find book!");
			}

		}

	}

	class ClickListener implements MouseListener {

		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			try {
				BooksController acc = new BooksController();
				String bookTitleDinTabel = theView.getIndexTabelBooks();
				String[] stringu = acc.getSearchedBook(bookTitleDinTabel);

				// test
				System.out.println(stringu[3]);
				theView.setFieldsBook(stringu[0], stringu[1], stringu[2],
						Integer.parseInt(stringu[3]),
						Double.parseDouble(stringu[4]));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Ceva n-o mers bine");
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}
	}

}
