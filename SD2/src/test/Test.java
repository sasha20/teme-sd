package test; 
import java.io.IOException;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class Test {
  
	public static void main(String[] args) throws JAXBException, IOException,
			XPathExpressionException, ParserConfigurationException,
			SAXException, TransformerException {

		// CREATE DATABASE
		// crud.createBooks();

		// DELETE BOOK by title
		// crud.deleteBook("Paspargilius");

		// UPDATE book by giving which book, what to update and with what
		// crud.updateBooks(0,"Name","Mirungere cu Oreste");

		// SEARCH a book by xml path
		// Book book= crud.searchBook("The Judge");

		// SEARCH a book by an attribute
		/*
		 * Book book = crud.searchTitle("Sasha Mihalache", "Author");
		 * System.out.println(book.getName()+" \n"+book.getAuthor() +
		 * " \n"+book.getIsbn()+ "\n"+ book.getPublisher()+"\n");
		 */

		// INSERT A BOOK
		// crud.insertBook("Paspargilius", "Sasha Mihalache","Stii pls",
		// "3132-adas");

		// READ all books from XML/////////////////////
		/*
		 * ArrayList<Book> list= crud.readBooks(); for(Book book : list){
		 * System.out.println("Book: "+ book.getName()+ " by "+ book.getAuthor()
		 * ); }
		 */

	}
}