package Model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model for the User object
 * 
 * @author Sasha
 * 
 */
@XmlRootElement(name = "user")
public class User {

	private String username;
	private String password;
	private int id;
	private String email;

	public User() {

	}

	public User(int id, String username, String password, String email) {
		this.id = id;

		this.username = username;
		this.password = password;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	@XmlElement
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	@XmlElement
	public void setPassword(String password) {
		this.password = password;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	@XmlAttribute
	public void setId(int id) {
		this.id = id;
	}

	/*
	 * public static void main(String[] args) throws FileNotFoundException,
	 * JAXBException, TransformerException { User us = new User();
	 * us.deleteUser("ronAldo"); }
	 */

	/*
	 * public static void main(String[] args) { ArrayList<User> list = new
	 * ArrayList<User>(); ArrayList<User> venire = new ArrayList<User>(); User
	 * us1 = new User(1,"admin","admin","admin@gmail.com"); User us2 = new
	 * User(2,"user", "user", "user@gmail.com"); User us3 = new
	 * User(3,"sasha","yosasha","sasha.mihalache@gmail.com"); User us4 ;
	 * list.add(us1); list.add(us2); list.add(us3); UserList listmare = new
	 * UserList(); listmare.setList(list);
	 * 
	 * marshall(listmare);
	 * 
	 * us4 = unmarshall("user", "user"); for (User user : venire){
	 * System.out.println("User: "+ user.getUsername() + " | Password: "+
	 * user.getPassword()); }
	 * 
	 * 
	 * }
	 */

}
