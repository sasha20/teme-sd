package main;

import Controller.LoginController;
import Service.UserCrud;
import View.LogView;

public class Main {

	/**
	 * Main class, Run from here Admin credentials : admin admin User
	 * credentials : user user
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LogView theView = new LogView();
		UserCrud theModel = new UserCrud();

		LoginController theController = new LoginController(theModel, theView);
		theView.setVisible(true);
	}

}

