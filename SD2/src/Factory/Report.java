package Factory;

/**
 * Inteface of Report
 * @author Sasha
 *
 */
public interface Report {
	/**
	 * Inherited method
	 */
   void generate();
}
