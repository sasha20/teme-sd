package Factory;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import Model.Book;
import Model.Bookstore;
import Model.UserList;
import Service.BookCrud;

/**
 * Done with marshalling with JAXB
 * 
 * @author Sasha
 * 
 */
public class ReportXml implements Report {

	/**
	 * Generates an XML report of the books in stock
	 */
	private static final String XML_FILE = "./reports/report.xml";

	@Override
	public void generate() {
		// TODO Auto-generated method stub
		// System.out.println("Generated a .xml file!");
		BookCrud crud = new BookCrud();
		ArrayList<Book> list = new ArrayList<Book>();
		ArrayList<Book> rez = new ArrayList<Book>();
		try {

			list = crud.readBooks();
			for (Book book : list) {
				if (book.getQuantity() == 0) {
					rez.add(book);
				}
			}
			Bookstore listmare = new Bookstore();
			listmare.setBookList(rez);

			File xmlfile = new File(XML_FILE);
			JAXBContext jaxbContext = JAXBContext.newInstance(Bookstore.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(listmare, xmlfile);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
