package Factory;


/**
 * Test class
 * @author Sasha
 *
 */
public class ReportDemo {

	public static void main(String[] args)
	{
		ReportFactory reportFactory = new ReportFactory();
		
		Report rep1 = reportFactory.getReport("TXT");
		rep1.generate();
		
		Report rep2 = reportFactory.getReport("XML");
		rep2.generate();
	}
}
