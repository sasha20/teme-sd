package Factory;

/**
 * The factory that selects between xml or txt requested file
 * @author Sasha
 *
 */
public class ReportFactory {

	public Report getReport(String reportType){
		if(reportType == null){
			return null;
		}
		if(reportType.equalsIgnoreCase("Txt")){
			return new ReportTxt();
		}
		if(reportType.equalsIgnoreCase("Xml")){
			return new ReportXml();
		}
		return null;
	}
}
