package Factory;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import Model.Book;
import Service.BookCrud;

/**
 * Used a Buffered Writer to write into a txt file by getting the data of out of
 * stock books from the Service(CRUD) package
 * 
 * @author Sasha
 * 
 */
public class ReportTxt implements Report {

	/**
	 * Generated a .txt report of the books in stock
	 */
	@Override
	public void generate() {
		BookCrud crud = new BookCrud();
		ArrayList<Book> list = new ArrayList<Book>();
		ArrayList<Book> rez = new ArrayList<Book>();
		try {

			list = crud.readBooks();
			for (Book book : list) {
				if (book.getQuantity() == 0) {
					rez.add(book);
				}
			}

			try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("./reports/report.txt"), "utf-8"))) {

				for (Book book : rez) {
					writer.write("Title: " + book.getTitle() + " Author: "
							+ book.getAuthor() + " Genre: " + book.getGenre()
							+ " Quantity: " + book.getQuantity() + " Price: "
							+ book.getPrice());
					writer.write("\n\n");
					writer.close();
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
