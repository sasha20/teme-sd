package Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList; 

import javax.swing.JOptionPane;
import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Model.Book;
import Model.Bookstore;

public class BookCrud {

	/**
	 * Method used for the GUI table to display all the books
	 * 
	 * @return - an Arraylist containg the bookstore and all its books
	 * @throws JAXBException
	 *             - if file is empty
	 * @throws FileNotFoundException
	 *             - if file is not found
	 */
	private static final String BOOKSTORE_XML = "./file/bookstore-jaxb.xml";

	public ArrayList<Book> readBooks() throws JAXBException,
			FileNotFoundException {
		try {

			JAXBContext context = JAXBContext.newInstance(Bookstore.class);
			Unmarshaller um = context.createUnmarshaller();
			Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
					BOOKSTORE_XML));
			ArrayList<Book> list = bookstore2.getBookList();

			if (list != null) {
				/*
				 * for(Book book: list){ //test //System.out.println("Book: "+
				 * book.getName() + " from " + book.getAuthor()); }
				 */
				return list;
			}
		} catch (Exception e) {
			System.out.println("File is empty");
		}
		return null;
	}

	/**
	 * Creates the database in XML format (used just for testing)
	 * 
	 * @throws JAXBException
	 */
	public void createBooks() throws JAXBException {
		ArrayList<Book> bookList = new ArrayList<Book>();
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		// create books
		Book book1 = new Book();
		book1.setPrice(8.22);
		book1.setQuantity(22);
		book1.setTitle("The Game");
		book1.setAuthor("Neil Strauss");
		book1.setGenre("Thriller");
		book1.setId(1);
		bookList.add(book1);

		Book book2 = new Book();
		book2.setPrice(9.42);
		book2.setQuantity(40);
		book2.setTitle("Castelul Vietii");
		book2.setAuthor("Sasha Mihalache");
		book2.setGenre("Sci-fi");
		book2.setId(2);
		bookList.add(book2);

		Book book3 = new Book();
		book3.setPrice(1.23);
		book3.setQuantity(10);
		book3.setTitle("The Judge");
		book3.setAuthor("Franz Kafka");
		book3.setGenre("Thriller");
		book3.setId(3);
		bookList.add(book3);

		// create bookstore, assigning books
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Libraria Universitatii");
		bookstore.setLocation("Cluj-Napoca");
		bookstore.setBookList(bookList);

		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(bookstore, System.out);
		m.marshal(bookstore, new File(BOOKSTORE_XML));

	}

	/**
	 * Search a book by giving attribute
	 * 
	 * @param whatToSearchFor
	 * @param tip
	 * @return
	 * @throws JAXBException
	 * @throws FileNotFoundException
	 */
	public ArrayList<Book> searchBook(String whatToSearchFor, String tip)
			throws JAXBException, FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));
		ArrayList<Book> rez = new ArrayList<Book>();
		ArrayList<Book> list = bookstore2.getBookList();

		for (Book book : list) {
			if (tip.equals("Title")) {
				if (book.getTitle().equals(whatToSearchFor))
					/*
					 * System.out.println("Book: "+ book.getTitle() + " from " +
					 * book.getAuthor() + " "+ book.getGenre()+ " " +
					 * book.getQuantity() + " " + book.getPrice() + "  ID:"+
					 * book.getId());
					 */
					rez.add(book);
			} else if (tip.equals("Author")) {
				if (book.getAuthor().equals(whatToSearchFor))
					rez.add(book);
			}
			else if(tip.equals("Genre")){
				if(book.getGenre().equals(whatToSearchFor))
					rez.add(book);
			}

		}
		
		return rez;
		
	}

	public Book searchTitle(String whatToSearchFor) throws JAXBException,
			FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));

		ArrayList<Book> list = bookstore2.getBookList();

		for (Book book : list) {
			if (book.getTitle().equals(whatToSearchFor)) {
				System.out.println("Book: " + book.getTitle() + " from "
						+ book.getAuthor() + " " + book.getGenre() + " "
						+ book.getQuantity() + " " + book.getPrice() + "  ID:"
						+ book.getId());
				// System.out.println("Book: "+ book.getName() + " from " +
				// book.getAuthor());
				return book;
			}
		}

		System.out.println(whatToSearchFor + " nu a fost gasita");
		return null;
	}

	/**
	 * Deletes a Book from the Bookstore by searching for it's title and
	 * updating the xml
	 * 
	 * @param bookName
	 *            - which Title to search for and delete if available
	 * @throws TransformerException
	 *             - daca nu poate updata xml-ul
	 */
	public static void deleteBook(String title) throws TransformerException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(BOOKSTORE_XML);
			doc.getDocumentElement().normalize();
			NodeList nodes = doc.getElementsByTagName("book");
			// System.out.println("Initial:");
			boolean ok = true;
			for (int i = 0; i < nodes.getLength(); i++) {
				// cu asta iau toate detaliile cartilor
				Element book = (Element) nodes.item(i);
				// System.out.println("Element: "+ (i+1) +
				// nodes.item(i).getTextContent());
				Element titles = (Element) book.getElementsByTagName("title")
						.item(0);
				String pName = titles.getTextContent();
				if (pName.equals(title)) {
					book.getParentNode().removeChild(book);
					System.out.println(pName + " o fo gasita si  stearsa :"
							+ book.getTextContent());
					System.out.println("``````````````````````````````");
					ok = false;
				}
			}
			/*
			 * System.out.println("La final:"); for(int j=0;
			 * j<nodes.getLength();j++)
			 * System.out.println(nodes.item(j).getTextContent());
			 */
			if (!ok) {
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(BOOKSTORE_XML));
				transformer.transform(source, result);
			} else {
				System.out.println("Cartea nu exista in baza");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Insert a new book in the .xml db If the name or isbn is already in the
	 * db, the user is notified
	 * 
	 * @param name
	 * @param author
	 * @param publisher
	 * @param isbn
	 * @throws FileNotFoundException
	 * @throws JAXBException
	 */
	public void insertBook(String title, String author, String genre,
			int quantity, double price) throws FileNotFoundException,
			JAXBException {
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));
		ArrayList<Book> list = bookstore.getBookList();
		boolean ok = true;
		int numar;
		for (Book bookie : list) {
			if (bookie.getTitle().equals(title)) {
				System.out.println("Exista deja carte, nu putem insera");
				ok = false;
			}
		}
		
		numar = list.get(list.size()-1).getId();
		
		if (ok) {
			Book book = new Book();
			book.setQuantity(quantity);
			book.setPrice(price);
			book.setTitle(title);
			book.setAuthor(author);
			book.setGenre(genre);
			book.setId(++numar);
			list.add(book);
			
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(bookstore, new File(BOOKSTORE_XML));
			System.out.println("Inserted: " + book.getTitle() + " by "
					+ book.getAuthor());
		}
	}

	/**
	 * Updates the requested book by giving details on which book to search from
	 * the comboboxes in the GUI Mai adauga cazuri
	 * 
	 * @param careCarte
	 * @param aladincombo
	 * @param updateStuff
	 */
	public void updateBooks(int id, String title, String author,
			String genre, int quantity, double price){
		try {
			// pt test

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(BOOKSTORE_XML);
			JAXBContext jc = JAXBContext.newInstance(Bookstore.class);
			Binder<Node> binder = jc.createBinder();
			binder.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Node xmlNode = doc.getDocumentElement();
			Bookstore bookstore = (Bookstore) binder.updateJAXB(xmlNode);
			
			BookCrud cv = new BookCrud();
			ArrayList<Book> list = cv.readBooks();
			for(Book book:list){
				if(book.getId() == id){
					book.setAuthor(author);
					book.setGenre(genre);
					book.setTitle(title);
					book.setPrice(price);
					book.setQuantity(quantity);
				}
			}
			bookstore.setBookList(list);
			 
			/*if (aladincombo.equals("Author") == true) {
				bookstore.getBookList().get(careCarte).setAuthor(updateStuff);
				System.out.println("You updated Author");
			}
			if (aladincombo.equals("Name") == true) {
				bookstore.getBookList().get(careCarte).setTitle(updateStuff);
				System.out.println("You updated Title");
			}
			*/
			// more cases

			xmlNode = binder.updateXML(bookstore);
			doc.setNodeValue(xmlNode.getNodeValue());

			// afisare in consola a update-ului si overwriting in .xml
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(doc), new StreamResult(new File(
					BOOKSTORE_XML)));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateBookQuantity( int id, int quantity ){
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(BOOKSTORE_XML);
			JAXBContext jc = JAXBContext.newInstance(Bookstore.class);
			Binder<Node> binder = jc.createBinder();
			binder.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Node xmlNode = doc.getDocumentElement();
			Bookstore bookstore = (Bookstore) binder.updateJAXB(xmlNode);
			
			BookCrud cv = new BookCrud();
			ArrayList<Book> list = cv.readBooks();
			for(Book book:list){
				if(book.getId() == id){
					 
					/*if(book.getQuantity()<1){
						JOptionPane.showMessageDialog(null, "Out of stock");
					}else*/
					book.setQuantity(book.getQuantity()-quantity);
				}
			}
			bookstore.setBookList(list);
			xmlNode = binder.updateXML(bookstore);
			doc.setNodeValue(xmlNode.getNodeValue());

			// afisare in consola a update-ului si overwriting in .xml
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(doc), new StreamResult(new File(
					BOOKSTORE_XML)));
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	/**
	 * Search for a book by title only with XPath
	 * 
	 * @param title
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	/*
	 * public Book searchTitle(String title) throws
	 * ParserConfigurationException, SAXException, IOException,
	 * XPathExpressionException{ DocumentBuilderFactory factory =
	 * DocumentBuilderFactory.newInstance(); DocumentBuilder builder =
	 * factory.newDocumentBuilder(); Document doc =
	 * builder.parse(BOOKSTORE_XML); XPathFactory xPathfactory =
	 * XPathFactory.newInstance(); XPath xpath = xPathfactory.newXPath();
	 * XPathExpression expr=
	 * xpath.compile("/bookstore/bookList/book/title/text()"); NodeList nl =
	 * (NodeList) expr.evaluate(doc, XPathConstants.NODESET); for(int i= 0;
	 * i<nl.getLength(); i++) { System.out.println(nl.item(i).getTextContent());
	 * 
	 * } return null;
	 * 
	 * }
	 */

}
