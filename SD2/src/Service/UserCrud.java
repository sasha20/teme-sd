package Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.bind.Binder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import Model.User;
import Model.UserList;

public class UserCrud {

	private static final String USER_XML = "./file/user-jaxb.xml";

	public static void marshall(UserList listmare) {

		try {
			File xmlfile = new File(USER_XML);
			JAXBContext jaxbContext = JAXBContext.newInstance(UserList.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(listmare, xmlfile);

			System.out.println("Success");
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	public static User unmarshall(String username, String password) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(UserList.class);
			Unmarshaller um = jaxbContext.createUnmarshaller();
			UserList listmare = (UserList) um
					.unmarshal(new FileReader(USER_XML));
			ArrayList<User> list = listmare.getList();
			for (User user : list) {
				if (user.getUsername().equals(username)
						&& user.getPassword().equals(password)) {
					System.out.println("l-am gasit");
					return user;

				}
			}
			System.out.println("nu l-am gasit");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	// returns all users in the database
	public ArrayList<User> readUsers() throws JAXBException,
			FileNotFoundException {
		try {
			JAXBContext context = JAXBContext.newInstance(UserList.class);
			Unmarshaller um = context.createUnmarshaller();
			UserList bookstore2 = (UserList) um.unmarshal(new FileReader(
					USER_XML));
			ArrayList<User> list = bookstore2.getList();

			if (list != null) {/*
								 * for(User book: list){ //test
								 * System.out.println("Username : "+
								 * book.getUsername() + " Password: " +
								 * book.getPassword()); }
								 */
				return list;
			}
		} catch (Exception e) {
			System.out.println("File is empty");
		}
		return null;
	}
	
	
	public User searchUser(String whatToSearchFor) throws JAXBException,
			FileNotFoundException {

		JAXBContext context = JAXBContext.newInstance(UserList.class);
		Unmarshaller um = context.createUnmarshaller();
		UserList bookstore2 = (UserList) um.unmarshal(new FileReader(USER_XML));

		ArrayList<User> list = bookstore2.getList();

		for (User user : list) {
			if (user.getUsername().equals(whatToSearchFor)) {
				System.out.println("Username: " + user.getUsername()
						+ " Password: " + user.getPassword() + " ID:"
						+ user.getId());
				return user;
			}
		}
		System.out.println(whatToSearchFor + " nu a fost gasita");
		return null;
	}

	public void updateUser(int id, String username, String password,
			String email) {
		try {
			// pt test

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(USER_XML);
			JAXBContext jc = JAXBContext.newInstance(UserList.class);
			Binder<Node> binder = jc.createBinder();
			binder.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			Node xmlNode = doc.getDocumentElement();
			
			UserCrud cv = new UserCrud();
			UserList bookstore = (UserList) binder.updateJAXB(xmlNode);
			ArrayList<User> list = cv.readUsers();
			for (User user : list) {
				if (user.getId() == id) {
					user.setUsername(username);
					user.setEmail(email);
					user.setPassword(password);
				}

			} 
			bookstore.setList(list);

			/*
			 * 
			 * if(aladincombo.equals("Author")==true){
			 * bookstore.getBookList().get(careCarte).setAuthor(updateStuff);
			 * System.out.println("You updated Author"); }
			 * if(aladincombo.equals("Name")==true){
			 * bookstore.getBookList().get(careCarte).setName(updateStuff);
			 * System.out.println("You updated Title"); } //more cases
			 */

			xmlNode = binder.updateXML(bookstore);
			doc.setNodeValue(xmlNode.getNodeValue());

			// afisare in consola a update-ului si overwriting in .xml
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(doc),
					new StreamResult(new File(USER_XML)));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertUser(String username, String password, String email)
			throws FileNotFoundException, JAXBException {
		JAXBContext context = JAXBContext.newInstance(UserList.class);
		Unmarshaller um = context.createUnmarshaller();
		UserList bookstore = (UserList) um.unmarshal(new FileReader(USER_XML));
		ArrayList<User> list = bookstore.getList();
		boolean ok = true;
		int numar;
		for (User bookie : list) {
			if (bookie.getUsername().equals(username)) {
				System.out
						.println("Exista deja userul acesta, nu putem insera");
				ok = false;
			}
		}

		numar = list.get(list.size() - 1).getId();

		if (ok) {
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(email);
			user.setId(++numar);

			list.add(user);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(bookstore, new File(USER_XML));
			System.out.println("Inserted: " + user.getUsername() + "  "
					+ user.getPassword() + " with ID: " + user.getId());
		}
	}

	public static void deleteUser(String bookName) throws TransformerException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(USER_XML);
			doc.getDocumentElement().normalize();
			NodeList nodes = doc.getElementsByTagName("user");
			// System.out.println("Initial:");
			boolean ok = true;
			for (int i = 0; i < nodes.getLength(); i++) {
				// cu asta iau toate detaliile cartilor
				Element book = (Element) nodes.item(i);
				// System.out.println("Element: "+ (i+1) +
				// nodes.item(i).getTextContent());
				Element titles = (Element) book
						.getElementsByTagName("username").item(0);
				String pName = titles.getTextContent();
				if (pName.equals(bookName)) {
					book.getParentNode().removeChild(book);
					System.out.println(pName + " o fo gasita si  stearsa :"
							+ book.getTextContent());
					System.out.println("``````````````````````````````");
					ok = false;
				}
			}
			/*
			 * System.out.println("La final:"); for(int j=0;
			 * j<nodes.getLength();j++)
			 * System.out.println(nodes.item(j).getTextContent());
			 */
			if (!ok) {
				TransformerFactory transformerFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(USER_XML));
				transformer.transform(source, result);
			} else {
				System.out.println("Userul nu exista in baza");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
