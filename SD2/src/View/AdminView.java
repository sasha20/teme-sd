package View;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import Controller.LoginController;
import Service.UserCrud;

/**
 * GUI for admin
 * 
 * @author Sasha
 * 
 */
public class AdminView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable jTableAccount;
	private JLabel lblWelcome;
	private JLabel lblUsername;
	private JTextField textFieldUsername;
	private JLabel lblPassword;
	private JTextField textFieldPassword;
	private JLabel lblRole;
	private JTextField textFieldRole;
	private JButton btnDelete;
	private JButton btnUpdateBook;
	private JButton btnInsertBook;
	private JButton btnCreateBook;
	private JButton btnDeleteBook;
	private JLabel lblAdminView;
	private JTabbedPane tabbedPane;
	private JButton btnUpdate;
	private JButton btnSave;
	private JPanel panel_1;
	private JTable table;
	private JTextField textFieldTitle;
	private JTextField textFieldAuthor;
	private JTextField textFieldGenre;
	private JTextField textFieldQuantity;
	private JTextField textFieldPrice;
	private JPanel panel_3;
	private JLabel lblSelectReport;
	private JComboBox comboBoxReport;
	private JLabel lblGenerateReport;
	private JButton btnGenerate;
	private JLabel lblViewYour;
	private JButton btnOpen;

	// private UserController

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	/*
	 * public void FillData(){ UserController a = new UserController(); dtm =
	 * a.tableUsers(); this.jTableAccount.setModel(dtm);
	 * 
	 * }
	 */
	public void welcome(String text) {
		this.lblWelcome.setText("Welcome " + text);
	}

	public AdminView() {
		setTitle("Admin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 601, 441);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 153, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblWelcome = new JLabel("Hi, ");
		lblWelcome.setBounds(31, 15, 163, 26);
		contentPane.add(lblWelcome);

		JButton btnBack = new JButton("Logout");
		btnBack.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					LogView theView = new LogView();
					UserCrud theModel = new UserCrud();
					LoginController theController = new LoginController(
							theModel, theView);
					theView.setVisible(true);
					setVisible(false);

				} catch (Exception x) {
					JOptionPane.showMessageDialog(null, x.getMessage());

				}

			}
		});
		btnBack.setBounds(486, 18, 89, 23);
		contentPane.add(btnBack);

		/**
		 * Delete Employee
		 */

		lblAdminView = new JLabel("Admin View");
		lblAdminView.setFont(new Font("Perpetua Titling MT", Font.BOLD, 17));
		lblAdminView.setBounds(231, 17, 137, 20);
		contentPane.add(lblAdminView);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(31, 47, 520, 344);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Users", null, panel, null);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 505, 127);
		panel.add(scrollPane);

		jTableAccount = new JTable();

		jTableAccount.setBackground(Color.WHITE);
		scrollPane.setViewportView(jTableAccount);
		jTableAccount.setModel(new DefaultTableModel(new Object[][] {
				{ null, null, null }, { null, null, null },
				{ null, null, null }, { null, null, null },
				{ null, null, null }, { null, null, null },
				{ null, null, null }, }, new String[] { "New column",
				"New column", "New column" }));

		JPanel accountInfo = new JPanel();
		accountInfo.setBounds(10, 152, 374, 153);
		panel.add(accountInfo);
		accountInfo.setBackground(new Color(173, 216, 230));
		accountInfo.setBorder(new TitledBorder(new CompoundBorder(
				new EtchedBorder(EtchedBorder.LOWERED, null, null),
				new TitledBorder(UIManager.getBorder("TitledBorder.border"),
						"", TitledBorder.LEADING, TitledBorder.TOP, null,
						new Color(0, 0, 0))), "Account Details",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		accountInfo.setLayout(null);

		lblUsername = new JLabel("Username :");
		lblUsername.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblUsername.setBounds(10, 28, 94, 26);
		accountInfo.add(lblUsername);

		textFieldUsername = new JTextField();
		textFieldUsername.setBounds(114, 30, 206, 20);
		accountInfo.add(textFieldUsername);
		textFieldUsername.setColumns(10);

		lblPassword = new JLabel("Password :");
		lblPassword.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblPassword.setBounds(10, 65, 94, 26);
		accountInfo.add(lblPassword);

		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(114, 67, 206, 20);
		accountInfo.add(textFieldPassword);
		textFieldPassword.setColumns(10);

		lblRole = new JLabel("Role :");
		lblRole.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblRole.setBounds(10, 103, 94, 20);
		accountInfo.add(lblRole);

		textFieldRole = new JTextField();
		textFieldRole.setBounds(114, 102, 206, 20);
		accountInfo.add(textFieldRole);
		textFieldRole.setColumns(10);

		btnUpdate = new JButton("Update");

		btnUpdate.setBounds(416, 214, 89, 23);
		panel.add(btnUpdate);
		btnUpdate.setFont(new Font("Stencil", Font.PLAIN, 11));

		btnSave = new JButton("Create");

		btnSave.setBounds(416, 160, 89, 23);
		panel.add(btnSave);
		btnSave.setFont(new Font("Stencil", Font.PLAIN, 11));
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnDelete.setBounds(416, 270, 89, 23);
		panel.add(btnDelete);
		btnDelete.setFont(new Font("Stencil", Font.PLAIN, 11));

		panel_1 = new JPanel();
		tabbedPane.addTab("Books", null, panel_1, null);
		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 495, 152);
		panel_1.add(scrollPane_1);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null }, }, new String[] {
				"New column", "New column", "New column", "New column",
				"New column" }));
		scrollPane_1.setViewportView(table);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new CompoundBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null), new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0))), "Books Info",
				TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBackground(new Color(176, 224, 230));
		panel_2.setBounds(20, 174, 370, 131);
		panel_1.add(panel_2);
		panel_2.setLayout(null);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 26, 46, 14);
		panel_2.add(lblTitle);

		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 62, 46, 14);
		panel_2.add(lblAuthor);

		JLabel lblGenre = new JLabel("Genre");
		lblGenre.setBounds(10, 103, 46, 14);
		panel_2.add(lblGenre);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(179, 46, 46, 14);
		panel_2.add(lblQuantity);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(179, 88, 46, 14);
		panel_2.add(lblPrice);

		textFieldTitle = new JTextField();
		textFieldTitle.setBounds(66, 23, 86, 20);
		panel_2.add(textFieldTitle);
		textFieldTitle.setColumns(10);

		textFieldAuthor = new JTextField();
		textFieldAuthor.setBounds(66, 59, 86, 20);
		panel_2.add(textFieldAuthor);
		textFieldAuthor.setColumns(10);

		textFieldGenre = new JTextField();
		textFieldGenre.setBounds(66, 100, 86, 20);
		panel_2.add(textFieldGenre);
		textFieldGenre.setColumns(10);

		textFieldQuantity = new JTextField();
		textFieldQuantity.setBounds(255, 43, 86, 20);
		panel_2.add(textFieldQuantity);
		textFieldQuantity.setColumns(10);

		textFieldPrice = new JTextField();
		textFieldPrice.setBounds(255, 85, 86, 20);
		panel_2.add(textFieldPrice);
		textFieldPrice.setColumns(10);

		btnCreateBook = new JButton("Create");
		btnCreateBook.setBounds(400, 174, 89, 23);
		panel_1.add(btnCreateBook);

		btnUpdateBook = new JButton("Update");
		btnUpdateBook.setBounds(400, 227, 89, 23);
		panel_1.add(btnUpdateBook);

		btnDeleteBook = new JButton("Delete");
		btnDeleteBook.setBounds(400, 282, 89, 23);
		panel_1.add(btnDeleteBook);

		panel_3 = new JPanel();
		tabbedPane.addTab("Reports", null, panel_3, null);
		panel_3.setLayout(null);

		lblSelectReport = new JLabel("1) Select Report Type :");
		lblSelectReport.setBounds(24, 44, 175, 32);
		panel_3.add(lblSelectReport);

		comboBoxReport = new JComboBox();
		comboBoxReport.setModel(new DefaultComboBoxModel(new String[] { "TXT",
				"XML" }));
		comboBoxReport.setBounds(209, 47, 99, 26);
		panel_3.add(comboBoxReport);

		lblGenerateReport = new JLabel("2) Generate Report :");
		lblGenerateReport.setBounds(24, 101, 175, 32);
		panel_3.add(lblGenerateReport);

		btnGenerate = new JButton("Generate");
		btnGenerate.setBounds(209, 104, 99, 27);
		panel_3.add(btnGenerate);

		lblViewYour = new JLabel("3) View your report :");
		lblViewYour.setBounds(24, 163, 175, 32);
		panel_3.add(lblViewYour);

		btnOpen = new JButton("Open");

		btnOpen.setBounds(209, 166, 99, 27);
		panel_3.add(btnOpen);

		// FillData();
	}

	public void setTable(DefaultTableModel dtm) {
		jTableAccount.setModel(dtm);
	}

	public void setTableBooks(DefaultTableModel dtm) {
		table.setModel(dtm);
	}

	public void setFields(String username, String password, String email) {
		textFieldUsername.setText(username);
		textFieldPassword.setText(password);
		textFieldRole.setText(email);
	}

	public void setFieldsBook(String title, String author, String genre,
			int quantity, double price) {
		textFieldTitle.setText(title);
		textFieldAuthor.setText(author);
		textFieldGenre.setText(genre);
		textFieldQuantity.setText(Integer.toString(quantity));
		textFieldPrice.setText(Double.toString(price));
	}

	public String getTitle() {
		return textFieldTitle.getText();
	}

	public String getAuthor() {
		return textFieldAuthor.getText();
	}

	public String getGenre() {
		return textFieldGenre.getText();
	}

	public String getQuantity() {
		return textFieldQuantity.getText();
	}

	public String getPrice() {
		return textFieldPrice.getText();
	}

	public String getUsername() {
		return textFieldUsername.getText();
	}

	public String getPassword() {
		return textFieldPassword.getText();
	}

	public String getEmail() {
		return textFieldRole.getText();
	}

	public String getCombo() {
		return (String) comboBoxReport.getSelectedItem();
	}

	// update listeners
	public void addUpdateListener(ActionListener listenForUpdate) {
		btnUpdate.addActionListener(listenForUpdate);
	}

	public void addUpdateListenerBook(ActionListener listenForUpdate) {
		btnUpdateBook.addActionListener(listenForUpdate);
	}

	// insert listeners
	public void addInsertListener(ActionListener listenForInsert) {
		btnSave.addActionListener(listenForInsert);
	}

	public void addInsertListenerBook(ActionListener listenForInsert) {
		btnCreateBook.addActionListener(listenForInsert);
	}

	// delete listeners
	public void addDeleteListener(ActionListener listenForDelete) {
		btnDelete.addActionListener(listenForDelete);
	}

	public void addDeleteListenerBook(ActionListener listenForDelete) {
		btnDeleteBook.addActionListener(listenForDelete);
	}

	public void addGenerateReport(ActionListener listenForReport) {
		btnGenerate.addActionListener(listenForReport);
	}

	public void addOpenReport(ActionListener listenForOpen) {
		btnOpen.addActionListener(listenForOpen);
	}

	// search in table for User
	public String getIndexTabel() {
		int index = jTableAccount.getSelectedRow();
		String username = jTableAccount.getValueAt(index, 0).toString();
		return username;
	}

	// search in table for Book
	public String getIndexTabelBooks() {
		int index = table.getSelectedRow();
		String title = table.getValueAt(index, 0).toString();
		return title;
	}

	// mouse listener for User table
	public void addMouseListener(MouseListener listerForClick) {

		// String[] stringu = acc.getSearchedUser(username);
		jTableAccount.addMouseListener(listerForClick);
	}

	// mouse listener for Book table
	public void addMouseListenerBook(MouseListener listenForClick) {
		table.addMouseListener(listenForClick);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminView frame = new AdminView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
}
