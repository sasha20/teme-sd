package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * GUI for logging in the system
 * 
 * @author Sasha
 * 
 */
public class LogView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldUsername;
	private JPasswordField passwordFieldPassword;
	private JButton btnLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogView frame = new LogView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogView() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 549, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.DARK_GRAY);
		panel_1.setForeground(new Color(0, 0, 0));
		panel_1.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Login System",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(255,
						255, 255)));
		panel_1.setBounds(106, 179, 318, 149);
		panel.add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 115, 61, 124, 0 };
		gbl_panel_1.rowHeights = new int[] { 41, 39, 36, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		btnLogin = new JButton("Login");
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setFont(new Font("Stencil", Font.PLAIN, 13));
		/*
		 * btnLogin.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) {
		 * 
		 * 
		 * String us = textFieldUsername.getText(); String pa =
		 * passwordFieldPassword.getText();
		 * 
		 * //Login l = new Login(); User a = new User(); if(us.equals("")||
		 * pa.equals("")) JOptionPane.showMessageDialog(null,
		 * "Empty Credentials"); else {
		 * 
		 * 
		 * // a = l.unmarshall(us, pa);
		 * 
		 * if( a ==null){ JOptionPane.showMessageDialog(null, "User not found");
		 * } else if(a.getId()==1 ){
		 * 
		 * Admin gg = new Admin(); //gg.welcome(user.getUsername());
		 * gg.setVisible(true); setVisible(false);
		 * 
		 * } else{ Regular f = new Regular(); // f.welcome(user.getUsername());
		 * f.setVisible(true); setVisible(false);
		 * 
		 * }
		 * 
		 * } } } );
		 */

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Stencil", Font.PLAIN, 13));
		lblUsername.setForeground(new Color(255, 255, 255));
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsername.gridx = 0;
		gbc_lblUsername.gridy = 0;
		panel_1.add(lblUsername, gbc_lblUsername);

		textFieldUsername = new JTextField();
		GridBagConstraints gbc_textFieldUsername = new GridBagConstraints();
		gbc_textFieldUsername.gridwidth = 2;
		gbc_textFieldUsername.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldUsername.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldUsername.gridx = 1;
		gbc_textFieldUsername.gridy = 0;
		panel_1.add(textFieldUsername, gbc_textFieldUsername);
		textFieldUsername.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Stencil", Font.PLAIN, 13));
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 0;
		gbc_lblPassword.gridy = 1;
		panel_1.add(lblPassword, gbc_lblPassword);

		passwordFieldPassword = new JPasswordField();
		GridBagConstraints gbc_passwordFieldPassword = new GridBagConstraints();
		gbc_passwordFieldPassword.gridwidth = 2;
		gbc_passwordFieldPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordFieldPassword.insets = new Insets(0, 0, 5, 0);
		gbc_passwordFieldPassword.gridx = 1;
		gbc_passwordFieldPassword.gridy = 1;
		panel_1.add(passwordFieldPassword, gbc_passwordFieldPassword);
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLogin.gridwidth = 2;
		gbc_btnLogin.gridx = 1;
		gbc_btnLogin.gridy = 2;
		panel_1.add(btnLogin, gbc_btnLogin);

	}

	public String getUsername() {
		return textFieldUsername.getText();
	}

	@SuppressWarnings("deprecation")
	public String getPassword() {
		return passwordFieldPassword.getText();
	}

	public void addLoginListener(ActionListener listenForLoginButton) {
		btnLogin.addActionListener(listenForLoginButton);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

}
