package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.SystemColor;
import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JButton;

import Controller.LoginController;
import Service.UserCrud;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.UIManager;

/**
 * Regular user GUI
 * 
 * @author Sasha
 * 
 */
public class UserView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel label;
	private JTable table;
	private JTextField textFieldSearch;
	private JTextField textFieldSell;
	private JTextField textFieldTitle;
	private JTextField textFieldAuthor;
	private JTextField textFieldQuantity;
	private JTextField textFieldPrice;
	private JTextField textFieldGenre;
	private JComboBox<String> comboBox;
	private JButton btnSearch;
	private JButton btnSell;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserView frame = new UserView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserView() {
		setTitle("User");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 608, 435);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(70, 130, 180));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		label = new JLabel("Hi, ");
		label.setBounds(10, 11, 163, 26);
		contentPane.add(label);

		JLabel lblEmployeeView = new JLabel("Employee View");
		lblEmployeeView.setFont(new Font("Perpetua Titling MT", Font.BOLD, 17));
		lblEmployeeView.setBounds(213, 38, 184, 20);
		contentPane.add(lblEmployeeView);

		JButton button = new JButton("Logout");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					LogView theView = new LogView();
					UserCrud theModel = new UserCrud();
					LoginController theController = new LoginController(
							theModel, theView);
					theView.setVisible(true);
					setVisible(false);

				} catch (Exception x) {
					JOptionPane.showMessageDialog(null, x.getMessage());

				}

			}
		});
		button.setFont(new Font("Stencil", Font.PLAIN, 11));
		button.setBounds(491, 14, 89, 23);
		contentPane.add(button);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 83, 570, 110);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null },
				{ null, null, null, null, null }, }, new String[] {
				"New column", "New column", "New column", "New column",
				"New column" }));
		scrollPane.setViewportView(table);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(135, 206, 250));
		panel.setBounds(10, 204, 570, 181);
		contentPane.add(panel);
		panel.setLayout(null);

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setForeground(new Color(0, 0, 0));
		separator.setBackground(new Color(0, 0, 0));
		separator.setBounds(400, 11, 8, 159);
		panel.add(separator);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(192, 192, 192));
		panel_1.setBorder(new TitledBorder(new CompoundBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null), null), "Sell (3)",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		panel_1.setBounds(418, 11, 142, 160);
		panel.add(panel_1);
		panel_1.setLayout(null);

		textFieldSell = new JTextField();
		textFieldSell.setBounds(37, 63, 71, 20);
		panel_1.add(textFieldSell);
		textFieldSell.setColumns(10);

		JLabel lblQuantity = new JLabel("Quantity:");
		lblQuantity.setBounds(50, 38, 51, 14);
		panel_1.add(lblQuantity);

		btnSell = new JButton("Sell");
		btnSell.setBounds(37, 113, 71, 23);
		panel_1.add(btnSell);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(192, 192, 192));
		panel_2.setBorder(new TitledBorder(null, "Search (1)",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 11, 380, 52);
		panel.add(panel_2);
		panel_2.setLayout(null);

		JLabel lblSearchBook = new JLabel("Search Book:");
		lblSearchBook.setBounds(10, 15, 78, 27);
		panel_2.add(lblSearchBook);

		textFieldSearch = new JTextField();
		textFieldSearch.setBounds(81, 18, 86, 20);
		panel_2.add(textFieldSearch);
		textFieldSearch.setColumns(10);

		JLabel lblBy = new JLabel("by:");
		lblBy.setBounds(177, 21, 46, 14);
		panel_2.add(lblBy);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Title",
				"Author", "Genre" }));
		comboBox.setBounds(203, 18, 78, 20);
		panel_2.add(comboBox);

		btnSearch = new JButton("Search");
		btnSearch.setBounds(303, 15, 67, 23);
		panel_2.add(btnSearch);

		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(192, 192, 192));
		panel_3.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Select Record (2)",
				TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		panel_3.setBounds(10, 83, 380, 88);
		panel.add(panel_3);
		panel_3.setLayout(null);

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(10, 25, 46, 14);
		panel_3.add(lblTitle);

		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setBounds(10, 50, 46, 14);
		panel_3.add(lblAuthor);

		textFieldTitle = new JTextField();
		textFieldTitle.setBounds(66, 22, 76, 20);
		panel_3.add(textFieldTitle);
		textFieldTitle.setColumns(10);

		textFieldAuthor = new JTextField();
		textFieldAuthor.setBounds(66, 47, 76, 20);
		panel_3.add(textFieldAuthor);
		textFieldAuthor.setColumns(10);

		JLabel lblQuantity_1 = new JLabel("Quantity:");
		lblQuantity_1.setBounds(152, 25, 46, 14);
		panel_3.add(lblQuantity_1);

		textFieldQuantity = new JTextField();
		textFieldQuantity.setBounds(208, 22, 76, 20);
		panel_3.add(textFieldQuantity);
		textFieldQuantity.setColumns(10);

		JLabel lblPrice = new JLabel("Price: ");
		lblPrice.setBounds(310, 25, 46, 14);
		panel_3.add(lblPrice);

		textFieldPrice = new JTextField();
		textFieldPrice.setBounds(308, 47, 62, 20);
		panel_3.add(textFieldPrice);
		textFieldPrice.setColumns(10);

		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(152, 50, 46, 14);
		panel_3.add(lblGenre);

		textFieldGenre = new JTextField();
		textFieldGenre.setBounds(208, 47, 76, 20);
		panel_3.add(textFieldGenre);
		textFieldGenre.setColumns(10);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(new Color(0, 0, 0));
		separator_2.setForeground(new Color(0, 0, 0));
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setBounds(290, 11, 10, 66);
		panel_3.add(separator_2);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(0, 0, 0));
		separator_1.setForeground(new Color(0, 0, 0));
		separator_1.setBounds(10, 72, 381, 11);
		panel.add(separator_1);
	}

	public void welcome(String text) {
		this.label.setText("Welcome, " + text);
	}

	public void setTableBooks(DefaultTableModel dtm) {
		table.setModel(dtm);
	}

	public void setFieldsBook(String title, String author, String genre,
			int quantity, double price) {
		textFieldTitle.setText(title);
		textFieldAuthor.setText(author);
		textFieldGenre.setText(genre);
		textFieldQuantity.setText(Integer.toString(quantity));
		textFieldPrice.setText(Double.toString(price));
	}

	public String getCombo(){
		return (String)comboBox.getSelectedItem();
	}
	
	public String getSearch(){
		return textFieldSearch.getText();
	}
	public String getTitle() {
		return textFieldTitle.getText();
	}

	public String getAuthor() {
		return textFieldAuthor.getText();
	}

	public String getGenre() {
		return textFieldGenre.getText();
	}

	public String getQuantity() {
		return textFieldQuantity.getText();
	}

	public String getPrice() {
		return textFieldPrice.getText();
	}

	public String getSell(){
		return textFieldSell.getText();
	}
	// search in table for Book
	public String getIndexTabelBooks() {
		int index = table.getSelectedRow();
		String title = table.getValueAt(index, 0).toString();
		return title;
	}

	// mouse listener for Book table
	public void addMouseListenerBook(MouseListener listenForClick) {
		table.addMouseListener(listenForClick);
	}

	// action listener for search button
	public void addSearchListener(ActionListener listenForSearch){
		btnSearch.addActionListener(listenForSearch);
	}
	
	//action listener for selling button
	public void addSellListener(ActionListener listenForSell){
		btnSell.addActionListener(listenForSell);
	}
}
