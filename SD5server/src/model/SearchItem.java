package model;

import java.io.Serializable;
import java.util.Date;

public class SearchItem implements Serializable {

	private String City;
	private Date fromDate;
	private Date toDate;
	
	
	public SearchItem(String City, Date fromDate, Date toDate){
		this.City = City;
		this.fromDate = fromDate;
		this.toDate = toDate;
		
	}


	public String getCity() {
		return City;
	}


	public void setCity(String city) {
		City = city;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	
	
	
}
