package model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;



@SuppressWarnings("serial")
public class User implements Serializable {


	private int id;
	private String username;
	private String password;
	private String type;
	private String telephone;
	private String firstName;
	private String lastName;
	private Date creationDate;
	private Set cons;
	
	public User(){}
	public User(int id){
		this.id =id;
	}
	
	 
	public User(  String username, String password, String type, String telephone, String firstName, String lastName, Date creationDate){
	 
		this.password=password;
		this.username=username;
		this.type=type;
		this.telephone = telephone;
		this.firstName = firstName;
		this.lastName = lastName;
		this.creationDate = creationDate;
	
	}
	
	
	
	public User( int id, String username,String password, String type, String telephone, String firstName, String lastName, Date creationDate ){
		this.id =id;
		this.password=password;
		this.username=username;
		this.type=type;
		this.telephone = telephone;
		this.firstName = firstName;
		this.lastName = lastName;
		this.creationDate = creationDate;
	
	}
	public int getId() {
		return id;
	}
 
	public void setId(int id) {
		this.id = id;
	}
 
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
 
	public String getPassword() {
		return password;
	}
 
	public void setPassword(String password) {
		this.password = password;
	}
 
	public String getType() {
		return type;
	}
 
	public void setType(String type) {
		this.type = type;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	 
	public Set getCons() {
		return cons;
	}
	public void setCons(Set cons) {
		this.cons = cons;
	} 
}
