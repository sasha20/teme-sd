package model;

import java.io.Serializable;

 

public class Rental implements Serializable{

	
	private int idrental;
	private int id_user;
	private float period;
	private String brand;
	private String model;
	private float price;
	private String firstName;
	private String lastName;
	
	
	public Rental(){}
	
	public Rental(int idrental){ this.idrental = idrental;}
	
	public Rental(float period, String model){
		this.period = period;
		this.model  = model;
	}
	
	public Rental(int idrental, int id_user, float period, String brand, String model, float price, String firstName, String lastName){
		this.idrental = idrental;
		this.id_user = id_user;
		this.period = period;
		this.brand = brand;
		this.model = model;
		this.price = price;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public Rental(int idrental,   float period, String brand, String model, float price, String firstName, String lastName){
		this.idrental = idrental;
	 
		this.period = period;
		this.brand = brand;
		this.model = model;
		this.price = price;
		this.firstName = firstName;
		this.lastName = lastName;
	}
 
	public Rental(  float period, String brand, String model, float price, String firstName, String lastName ,int id_user ){
		this.id_user = id_user;
	 
		this.period = period;
		this.brand = brand;
		this.model = model;
		this.price = price;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public int getIdrental() {
		return idrental;
	}

	public void setIdrental(int idrental) {
		this.idrental = idrental;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public float getPeriod() {
		return period;
	}

	public void setPeriod(float period) {
		this.period = period;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public boolean equals(Object obj){
		if(obj ==null) return false;
		if(!this.getClass().equals(obj.getClass())) return false;
		
		Rental obj2 = (Rental) obj;
		if((this.idrental == obj2.getIdrental() && (this.firstName.equals(obj2.getFirstName())))){
			return true;
		}
		return false;
	}
	
	public int hashCode(){
		int tmp = 0;
		tmp = (idrental + firstName).hashCode();
		return tmp;
	}
	
}
