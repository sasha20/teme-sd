package Utility;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import view.ViewDrawingBoard;
import model.DrawingBoard;

public class FileOps {

	
	
	public void saveImage(Graphics2D g2, DrawingBoard drawB) {
	 
		 try {
		        BufferedImage image = new BufferedImage(drawB.getWidth(),
		                drawB.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		        Graphics g = image.getGraphics();
		        drawB.printAll(g);
		        g.dispose();
		        ImageIO.write(image, "png", new File("saves/img.png"));
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		
    }
	
	public void loadImage(DrawingBoard drawB , JFrame frame){
		ImageIcon image = new ImageIcon("saves/img.png");
		//JLabel label = new JLabel("", image, JLabel.CENTER);
		JLabel label =new JLabel();
		label.setIcon(image);
		
	 
		//frame.setVisible(true);
		
		DrawingBoard newcanvas = new DrawingBoard();
		newcanvas.add(label);
		
		ViewDrawingBoard newboard = new  ViewDrawingBoard(newcanvas);
		newboard.setVisible(true);
		newboard.pack();
		frame.setVisible(false);
		
		//return drawB;
	}
	
}
