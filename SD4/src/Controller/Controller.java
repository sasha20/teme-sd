package controller;

import java.awt.Color;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.text.DecimalFormat;

import javax.swing.JColorChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Utility.FileOps;
import model.DrawingBoard;
import model.DrawingFunctions;
import model.PolyLine;
import view.ViewDrawingBoard;

public class Controller {
	
	
	private DrawingBoard drawB = new DrawingBoard();
	public ViewDrawingBoard viewDrawB = new ViewDrawingBoard(drawB);
	private Color strokeColor = Color.BLACK;
	private Color fillColor = Color.BLACK;
	private float transparentVal = 1.0f;
	private DecimalFormat dec = new DecimalFormat("#.##");
	private FileOps op;
	
	public Controller(){

		viewDrawB.addButtonActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource()==viewDrawB.getBtnBrush()){
						drawB.setCurrentAction(1);
						//System.out.println("CRTACC="+currentAction);
				}
			}
		});
		viewDrawB.addButtonActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource()==viewDrawB.getBtnLine()){
					drawB.setCurrentAction(2);
				//System.out.println("CRTACC="+currentAction);
				}
			}
		});
		viewDrawB.addButtonActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource()==viewDrawB.getBtnEllipse()){
					drawB.setCurrentAction(3);
				}
			}
		});
		viewDrawB.addButtonActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource()==viewDrawB.getBtnRectangle()){
					drawB.setCurrentAction(4);
				}
			}
		});
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == viewDrawB.getBtnPen()){
					drawB.setCurrentAction(5);
				}
			}
			
			
		});
		
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if(arg0.getSource() == viewDrawB.getBtnContour()){
					strokeColor = JColorChooser.showDialog(null, "Pick a Contour Color", Color.BLACK);
				}
				 
			}
			
		});
		
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				 if(arg0.getSource() == viewDrawB.getBtnColor()){
				    fillColor = JColorChooser.showDialog(null, "Pick a Filling Color", Color.BLACK);
				 }
				}
			
		});
		
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				 if(arg0.getSource() == viewDrawB.getBtnNewPage()){
				    drawB.getShapes().clear();
				    drawB.lines.clear();
					drawB.getShapeFill().clear();
				    drawB.getShapeStroke().clear();
				    drawB.getTransPercent().clear();
				    drawB.repaint();
				 }
				}
			
		});
		
		viewDrawB.addMyChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if(e.getSource() == viewDrawB.getTranpSlider()){
					viewDrawB.setTranspLabel("Opacity: "+dec.format(viewDrawB.getTranpSlider().getValue()*.01));
					transparentVal = (float) (viewDrawB.getTranpSlider().getValue() * .01);
					 
				}
			}
			
			
			
		});
		
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				 if(arg0.getSource() == viewDrawB.getSave()){
						 
						 op = new FileOps();
						 op.saveImage(drawB.getGraphics(), drawB);
				 }
			}
			
			
		});
		viewDrawB.addButtonActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.getSource() == viewDrawB.getLoad()){ 
					op = new FileOps();
					  drawB.getShapes().clear();
					    drawB.lines.clear();
					    drawB.repaint();
					
				  op.loadImage(drawB , viewDrawB.getFrame());
				   
				    
				}
			}
			
			
		});
		
		
		
		
		final DrawingFunctions myFunctions = new DrawingFunctions();
		
		drawB.addMyMouseActivity(new MouseAdapter() {
		      public void mousePressed(MouseEvent e){
		    	 
		    	  
		    	  if(drawB.getCurrentAction() != 1 && drawB.getCurrentAction() !=5)
		    	  {
		    		  drawB.setDrawStart(new Point(e.getX(), e.getY()));
		    		  drawB.setDrawEnd(drawB.getDrawStart());
		    		  drawB.repaint();
		    	//	  System.out.println("PRESSED:"+drawB.getDrawStart());
		    	  }
		    	  
		    	  //pt pen
		    	  else if(drawB.getCurrentAction() == 5){
		    		  
		    		  drawB.currentLine = new PolyLine();
		    		  drawB.lines.add(drawB.currentLine);
		    		  drawB.currentLine.addPoint(e.getX(), e.getY());
		    	  }
		      }
		      
		      
		      public void mouseReleased(MouseEvent e){
		    	  
		    	  if(drawB.getCurrentAction()!=1 && drawB.getCurrentAction()!=5){
		    	  
		    	  Shape aShape = null; 
		    	  if(drawB.getCurrentAction()==2)
		    	  {
		    		  aShape=myFunctions.drawLine(drawB.getDrawStart().x, drawB.getDrawStart().y, e.getX(), e.getY());
		    	  }
		    	  else if(drawB.getCurrentAction()==3)
		    	  {	    		  
                  		aShape = myFunctions.drawEllipse(drawB.getDrawStart().x, drawB.getDrawStart().y, e.getX(), e.getY());        	
		    	  }
		    	  else if(drawB.getCurrentAction()==4)
		    	  {	    		  
                  		aShape = myFunctions.drawRectangle(drawB.getDrawStart().x, drawB.getDrawStart().y,	e.getX(), e.getY());        	
		    	  }
		    	 
		    	  
			    	  drawB.getShapes().add(aShape);
			    	  drawB.getShapeFill().add(fillColor);
			    	  drawB.getShapeStroke().add(strokeColor);
			    	  
			    	  drawB.getTransPercent().add(transparentVal);
			    	  drawB.setDrawStart(null);
			    	  drawB.setDrawEnd(null);
			    	  
			    	  drawB.repaint();
		    	  }
		    	  
		    	  else if (drawB.getCurrentAction() == 5){
		    		   drawB.currentLine.addPoint(e.getX(), e.getY());
		               drawB.repaint();  // invoke paintComponent()
		    		  
		    	  }
		    	  
		      }
		});
		
		
		
		
		drawB.addMyMotionActivity(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e){
			if (drawB.getCurrentAction()==1){	
				int x = e.getX();
				int y = e.getY();
				
				Shape aShape=null; 
				strokeColor=fillColor; 
				aShape = myFunctions.drawBrush(x,y,5,5);
				
				drawB.getShapes().add(aShape);
				drawB.getShapeFill().add(fillColor);
				drawB.getShapeStroke().add(strokeColor);
				drawB.getTransPercent().add(transparentVal);
				
//				drawB.setDrawStart(null);
//				drawB.setDrawEnd(null);
//				drawB.repaint();
			}
			else if (drawB.getCurrentAction()==5){	
 
				drawB.currentLine.addPoint(e.getX(), e.getY());
	           // drawB.repaint();  // invoke paintComponent()
				
			}
			
			
			
			
			drawB.setDrawEnd(new Point(e.getX(),e.getY()));
			drawB.repaint();
			}
		});
	}
	



}
