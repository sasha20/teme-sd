package model;

import java.awt.Graphics;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

public class Brush extends Shape {

	private List<Integer> xList;
	private List<Integer> yList;
	
	public Brush(){
		super();
		xList = new ArrayList<Integer>();
		yList = new ArrayList<Integer>();
	}
	
	
	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		for(int i=0; i<xList.size() -1 ; ++i){
			
			
		}
	}

	
	
    private Ellipse2D.Float drawEllipse(
            int x1, int y1, int x2, int y2)
    {
            int x = Math.min(x1, x2);
            int y = Math.min(y1, y2);
            int width = Math.abs(x1 - x2);
            int height = Math.abs(y1 - y2);

            return new Ellipse2D.Float(
                    x, y, width, height);
    }
    private Ellipse2D.Float drawBrush(
            int x1, int y1, int brushStrokeWidth, int brushStrokeHeight)
    {
    	
    	return new Ellipse2D.Float(
                x1, y1, brushStrokeWidth, brushStrokeHeight);
    	
    }
}
