package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;

import model.DrawingBoard;
import java.awt.Color;
import java.awt.SystemColor;

public class ViewDrawingBoard extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private JPanel contentPane;
	private JPanel bottomPanel;
	
	private JButton btnLine;
	private JButton btnEllipse;
	private JButton btnRectangle;
	private JButton btnBrush;
	private JButton btnColor;
	private JButton btnPen;
	private JButton btnContour;
	private JButton btnNewPage;
	
	
	
	private ImageIcon iconLine;
	private ImageIcon iconEllipse;
	private ImageIcon iconRectangle;
	private ImageIcon iconBrush;
	private ImageIcon iconColor;
	private ImageIcon iconPen;
	private ImageIcon iconContour;
	private ImageIcon iconNewPage;
	
	private JLabel transparencyLabel; 
	private JSlider transparencySlider;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmSave;
	private JMenuItem mntmLoad;
	
	
	
	
	public ViewDrawingBoard(DrawingBoard myDrawingBoard){
		
	 
		
		setTitle("Sasha's Paint");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		
		bottomPanel=new JPanel();
		bottomPanel.setBackground(SystemColor.activeCaption);
		//bottomPanel.setBounds(383, 543, 115, 19);
		
//		contentPane = new JPanel(); 
//		contentPane.setBorder(new EmptyBorder(5,5,5,5));
//		setContentPane(contentPane);
//		contentPane.setLayout(new BorderLayout());
		
		 iconLine = new ImageIcon("resources/line.png");
		 iconEllipse = new ImageIcon("resources/ellipse.png");
		 iconRectangle = new ImageIcon("resources/rectangle.png");
		 iconBrush = new ImageIcon("resources/brush.png");
		 iconPen = new ImageIcon("resources/pen.png");
		 iconColor = new ImageIcon("resources/color.png");
		 iconContour = new ImageIcon("resources/contour.png");
		 iconNewPage = new ImageIcon("resources/refresh.png");
		 
		btnLine = new JButton(iconLine);
	 
		//contentPane.setLayout(null);
		btnLine.setBounds(236, 492, 70, 70);
		//contentPane.add(btnLine);
		
		btnEllipse = new JButton(iconEllipse);
		btnEllipse.setBounds(334, 492, 70, 70);
		//contentPane.add(btnEllipse);
		
		btnRectangle = new JButton(iconRectangle);
		btnRectangle.setBounds(442, 492, 70, 70);
		//contentPane.add(btnRectangle);
		
		btnColor = new JButton(iconColor);
		btnContour = new JButton(iconContour);
		btnNewPage = new JButton(iconNewPage);
		
		
		btnPen = new JButton(iconPen);
		//btnPen.setBounds(442, 492, 70, 70);
		
		btnBrush = new JButton(iconBrush);
		
		transparencyLabel=new JLabel("Opacity: 1");
		transparencySlider=new JSlider(1,99,99);
		
		
		
		getContentPane().add(myDrawingBoard, BorderLayout.CENTER);
		
		  
		bottomPanel.add(btnLine);
		bottomPanel.add(btnEllipse);
		bottomPanel.add(btnRectangle);
		bottomPanel.add(btnBrush);
		bottomPanel.add(btnPen);
		bottomPanel.add(btnContour);
		bottomPanel.add(btnColor);
		bottomPanel.add(btnNewPage);
		bottomPanel.add(transparencyLabel);
		bottomPanel.add(transparencySlider);
	
		bottomPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		// bottomPanel.setBorder(new TitledBorder(new MatteBorder(new ImageIcon("")), "Paint Tools"));
		
		 
		
//		theBox.setSize(100,100);
//		bottomPanel.setSize(1000,1000);
		getContentPane().add(bottomPanel, BorderLayout.NORTH);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		mntmLoad = new JMenuItem("Load");
		mnFile.add(mntmLoad);
		
		
		
		
		
		}
	public JButton getBtnLine() {
		return btnLine;
	}
	public void setBtnLine(JButton btnLine) {
		this.btnLine = btnLine;
	}
	public JButton getBtnEllipse() {
		return btnEllipse;
	}
	public void setBtnEllipse(JButton btnEllipse) {
		this.btnEllipse = btnEllipse;
	}
	public JButton getBtnRectangle() {
		return btnRectangle;
	}
	public void setBtnRectangle(JButton btnRectangle) {
		this.btnRectangle = btnRectangle;
	}
	public JButton getBtnBrush() {
		return btnBrush;
	}
	public void setBtnBrush(JButton btnBrush) {
		this.btnBrush = btnBrush;
	}
	public JButton getBtnPen(){
		return btnPen;
	}
	public void setBtnPen(JButton btnPen){
		this.btnPen = btnPen;
	}
	public JButton getBtnColor(){
		return btnColor;
		
	}
	public JButton getBtnContour(){
		return btnContour;
	}
	public void setBtnColor(JButton btnColor){
		this.btnColor = btnColor;
	}
	public void setBtnContour(JButton btnContour){
		this.btnContour = btnContour;
	}
	public void setBtnNewPage(JButton btnNewPage){
		this.btnNewPage = btnNewPage;
	}
	public JButton getBtnNewPage(){
		return btnNewPage; 
	}
	
	public JLabel getTranspLabel(){
		return transparencyLabel;
	}
	
	public void setTranspLabel(String text){
		this.transparencyLabel.setText(text);
	}
	
	public JSlider getTranpSlider(){
		return transparencySlider;
	}
	
	public void setTranspSlider(JSlider transpSlider){
		this.transparencySlider = transpSlider;
	}
	public void addMyChangeListener(ChangeListener listener){
		transparencySlider.addChangeListener(listener);
	}
	
	public JMenuItem getSave(){
		return mntmSave;
	}
	
	public JMenuItem getLoad(){
		return mntmLoad;
	}
	
	public JFrame getFrame(){
		return this;
	}
	
	public void addButtonActionListener(ActionListener listener){
		btnLine.addActionListener(listener);
		btnEllipse.addActionListener(listener);
		btnRectangle.addActionListener(listener);
		btnBrush.addActionListener(listener);
		btnPen.addActionListener(listener);
		btnContour.addActionListener(listener);
		btnColor.addActionListener(listener);
		btnNewPage.addActionListener(listener);
		mntmSave.addActionListener(listener);
		mntmLoad.addActionListener(listener);
		
	}
}





