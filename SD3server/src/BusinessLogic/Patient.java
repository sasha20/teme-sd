package BusinessLogic;



import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class Patient implements Serializable{
		private int id;
		private String name;
		private int cnp;
		private String address;
		private Date birth;
		private String series;
		private Set consult;

	public Patient(){};
	public Patient(int id) { this.id = id;}
	public Patient(String name, String series, int cnp, String address, Date birth)
	{
		 
		this.name=name;
		this.cnp=cnp;
		this.address=address;
		this.series = series;
		this.birth=birth;
	}
	public Patient(int id, String name, String series, int cnp, String address, Date birth)
	{
		this.id = id;
		this.name=name;
		this.cnp=cnp;
		this.address=address;
		this.series = series;
		this.birth=birth;
	}
	
 
	@Override
	public String toString() {
		return "\nPacientul: " + name +"\nID-ul pacientului: " + id + "\nSerie Buletin:  "+ series +  "\nCNP-ul: " + cnp
				+ "\nAdresa: " + address + "\nData nasterii: " +birth ;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCnp() {
		return cnp;
	}
	public void setCnp(int cnp) {
		this.cnp = cnp;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public Set getConsult() {
		return consult;
	}
	public void setConsult(Set consult) {
		this.consult = consult;
	}

	
}