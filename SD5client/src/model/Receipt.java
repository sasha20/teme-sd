package model;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
public class Receipt {

	 
	private Date time; 
	private String  firstName;
	private String  lastName;
    private float period;
    private String  model;
	private String  brand;
	private float price;
	
	public Receipt(String firstName,String lastName, float period, String brand, String model, float price  )
	{
		this.firstName = firstName;
		this.lastName = lastName; 
		this.brand = brand;
		this.model = model;
		this.period = period;
		this.price = price; 
		this.time = new Date();
		 
	}
	 
	public void printPDF()
	{
		try{
			File file = new File("receipt.pdf");
			FileOutputStream fileout = new FileOutputStream(file);
			Document document = new Document();
			
			PdfWriter.getInstance(document, fileout);
			document.addAuthor("Sasha");
			document.addTitle("PDF print");
			
			document.open();
			
			Chunk chunk = new Chunk ("Sasha's Rent-A-Car");
			Font font = new Font(Font.COURIER);
			font.setStyle(Font.UNDERLINE);
			font.setStyle(Font.ITALIC);
			font.setSize(20);
			chunk.setFont(font);
			//chunk.setBackground(Color.BLACK);
			document.add(chunk);
			Image image;
			
			try{
				image = Image.getInstance("pics/service.jpg");
				image.setAlignment(Image.MIDDLE);
				document.add(image);
			}
			catch(MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Paragraph paragraph = new Paragraph();
			paragraph.add(this.toString());
			paragraph.setAlignment(Element.ALIGN_CENTER);
			document.add(paragraph);
			  
			document.close();
			}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		 
		
		}
	 
	
	public String toString()
	{
		return "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n"
				+ "First Name: "+ firstName + "\nLast Name: "+lastName+ "\nPeriod: "+period+
				
				"\nBrand: "+brand+ "\nModel: "+ model + " \n\nPrice: " + price + " for " + period +" days"+
				"\nContract created on:"+ new Date()+ "\n\nSign here .............................\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	}
}
