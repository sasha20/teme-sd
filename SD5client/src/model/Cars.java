package model;

import java.io.Serializable;
import java.util.Date;

public class Cars implements Serializable{

	private int idcars;
	private String brand;
	private String model;
	private int year;
	private float rentingPrice;
	private int kilometers;
	private String picture;
	private String availability;
	private Date fromDate;
	private Date toDate;
	private String city;

	
	public Cars(){}
	public Cars(int idcars){
		this.idcars = idcars;
	}
	public Cars(String model){
		this.model = model;
		
	}
	
	 public Cars(int idcars, String brand, String model, int year, float rentingPrice, int kilometers, String picture, String availability, Date from, Date to) {
	        this.idcars = idcars;
	        this.brand = brand;
	        this.model = model;
	        this.year = year;
	        this.rentingPrice = rentingPrice;
	        this.kilometers = kilometers;
	        this.picture = picture;
	        this.availability = availability;
	        this.fromDate = from;
	        this.toDate  = to;
	    }
	 public Cars( String brand, String model, int year, float rentingPrice, int kilometers, String picture, String availability, Date from, Date to, String city) {
	        
	        this.brand = brand;
	        this.model = model;
	        this.year = year;
	        this.rentingPrice = rentingPrice;
	        this.kilometers = kilometers;
	        this.picture = picture;
	        this.availability = availability;
	        this.fromDate = from;
	        this.toDate  = to;
	        this.city = city;
	    }
	 
	 
	 public Cars(int idcars, String brand, String model, int year, float rentingPrice, int kilometers,   String availability, Date from, Date to, String city) {
	        this.idcars = idcars;
	        this.brand = brand;
	        this.model = model;
	        this.year = year;
	        this.rentingPrice = rentingPrice;
	        this.kilometers = kilometers;
	        this.city = city;
	        this.availability = availability;
	        this.fromDate = from;
	        this.toDate  = to;
	    }
	 

	 public Cars( String brand, String model, int year, float rentingPrice, int kilometers,   String availability, Date from, Date to) {
	        
	        this.brand = brand;
	        this.model = model;
	        this.year = year;
	        this.rentingPrice = rentingPrice;
	        this.kilometers = kilometers;
	        
	        this.availability = availability;
	        this.fromDate = from;
	        this.toDate  = to;
	    }
	 
	public int getIdcars() {
		return idcars;
	}
	public void setIdcars(int idcars) {
		this.idcars = idcars;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public float getRentingPrice() {
		return rentingPrice;
	}
	public void setRentingPrice(float rentingPrice) {
		this.rentingPrice = rentingPrice;
	}
	public int getKilometers() {
		return kilometers;
	}
	public void setKilometers(int kilometers) {
		this.kilometers = kilometers;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
 
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
}
