package Presentation;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import BusinessLogic.User;
import Connection.Client;

import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdminView extends JFrame {

	private JPanel contentPane;
	private static JTable table;
	private JTextField textFieldUsername;
	private JTextField textFieldPassword;
	private JTextField textFieldRole;
	private int numar;
	private JTextField textFieldNumeReal;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminView frame = new AdminView();
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					frame.setVisible(true);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void FillData(){ 
		 try{ 
			 	Client.connect();
			 	DefaultTableModel dtm = null ; 
			 	Client.passMessageToServer("citireUser");
			 	Client.passTableToServer(dtm);
			 	Object obj=Client.receiveTableFromServer();
			 	dtm=(DefaultTableModel)obj; 
			 	table.setModel(dtm);
		   	 	String fmm = Client.receiveMessageFromServer();
		   
		   	 System.out.println("Mesajul din FillDAta" +  fmm);
		   
		 }catch(Exception e){ e.printStackTrace();}	
		}
	
	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public AdminView() throws UnknownHostException, IOException {
		setTitle("Admin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 637, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				LogInView gg = new LogInView();
				gg.setVisible(true);
				
			}
		});
		btnLogout.setBounds(531, 11, 80, 29);
		contentPane.add(btnLogout);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 69, 601, 251);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Users", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 260, 201);
		panel.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int index=  table.getSelectedRow();
				String username = table.getValueAt(index, 0).toString();
				try{
					
					Client.connect();		
					User user = new User(username, null,null,null);
					Client.passMessageToServer("search");
					Client.passObjectToServer(user);
					Object obj=Client.receiveObjectFromServer();
					user=(User)obj;
				
					textFieldPassword.setText(user.getPassword());
					textFieldRole.setText(user.getType());
					textFieldUsername.setText(user.getName());
					textFieldNumeReal.setText(user.getNumeReal());
					numar = user.getId();
					System.out.println("ID: "+numar);
					String fmm = Client.receiveMessageFromServer();
					System.out.println("Mesaju de la click pe table: " + fmm);
				}
				
				catch(Exception e){e.printStackTrace();};
				
			}
		});
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblUsername = new JLabel("Username: ");
		lblUsername.setBounds(307, 14, 64, 22);
		panel.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(307, 47, 64, 22);
		panel.add(lblPassword);
		
		JLabel lblRole = new JLabel("Role:");
		lblRole.setBounds(307, 80, 64, 22);
		panel.add(lblRole);
		
		textFieldUsername = new JTextField();
		textFieldUsername.setBounds(409, 15, 86, 20);
		panel.add(textFieldUsername);
		textFieldUsername.setColumns(10);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(409, 48, 86, 20);
		panel.add(textFieldPassword);
		textFieldPassword.setColumns(10);
		
		textFieldRole = new JTextField();
		textFieldRole.setBounds(409, 80, 86, 20);
		panel.add(textFieldRole);
		textFieldRole.setColumns(10);
		
		JButton btnInsertUser = new JButton("Insert");
		btnInsertUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					String pass = textFieldPassword.getText();
					String username = textFieldUsername.getText();
					String role = textFieldRole.getText();
					String numeReal= textFieldNumeReal.getText();
					if(username.equals("") || role.equals("") || pass.equals("") || numeReal.equals(""))
					{
						JOptionPane.showMessageDialog(null, "Empty fields!");
					}
					else if(!role.equals("admin") && !role.equals("secretary") && !role.equals("doctor"))  {
						JOptionPane.showMessageDialog(null, "Type can be only admin / secretary / doctor");
					}
					else{
				 	User user = new User(username,pass, role , numeReal);
				 		Client.connect();
						Client.passMessageToServer("inserareUser");
						Client.passObjectToServer(user); 
						String fmm = Client.receiveMessageFromServer();
						System.out.println("Mesaj din Insert Button: "+ fmm); 
						FillData(); 
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
			}
		});
		btnInsertUser.setBounds(307, 174, 74, 23);
		panel.add(btnInsertUser);
		
		JButton btnUpdateUser = new JButton("Update");
		btnUpdateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try{
					String pass = textFieldPassword.getText();
					String username = textFieldUsername.getText();
					String role = textFieldRole.getText();
					String numeReal= textFieldNumeReal.getText();
					if(username.equals("") || role.equals("") || pass.equals("") || numeReal.equals(""))
					{
						JOptionPane.showMessageDialog(null, "Empty fields!");
					}
					else if(!role.equals("admin") && !role.equals("secretary") && !role.equals("doctor"))  {
						JOptionPane.showMessageDialog(null, "Type can be only admin / secretary / doctor");
					}
					else{
						User user = new User(numar, username,pass, role, numeReal);
						Client.connect();
						Client.passMessageToServer("updateUser");
						Client.passObjectToServer(user); 
						String fmm = Client.receiveMessageFromServer();
						System.out.println("Mesaj din Update Button: "+ fmm); 
						FillData(); 
					 
					}
					
				}catch(Exception e){
					e.printStackTrace();
				} 
			}
		});
		btnUpdateUser.setBounds(409, 174, 74, 23);
		panel.add(btnUpdateUser); 
		JButton btnDeleteUser = new JButton("Delete");
		btnDeleteUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 try{
					 
					 int result = JOptionPane.showConfirmDialog(null, "Are you sure?","Confirm", JOptionPane.YES_NO_OPTION);
				 
					 if(result==JOptionPane.YES_OPTION)
					 { 
						 	User user = new User(numar);
							Client.connect();
							Client.passMessageToServer("deleteUser");
							Client.passObjectToServer(user); 
							String fmm = Client.receiveMessageFromServer();
							System.out.println("Mesaj din Delete Button: "+ fmm); 
							FillData();  
					 } 
					 }
				 catch(Exception xxx){
					 JOptionPane.showMessageDialog(null, "Select a field in the table first.");
				 }
				
			}
		});
		btnDeleteUser.setBounds(512, 174, 74, 23);
		panel.add(btnDeleteUser);
		
		JLabel lblName = new JLabel("Name: ");
		lblName.setBounds(307, 113, 46, 14);
		panel.add(lblName);
		
		textFieldNumeReal = new JTextField();
		textFieldNumeReal.setBounds(409, 111, 86, 20);
		panel.add(textFieldNumeReal);
		textFieldNumeReal.setColumns(10);
		
		JLabel lblAdmin = new JLabel("Admin");
		lblAdmin.setBounds(10, 18, 61, 22);
		contentPane.add(lblAdmin);
		
		//FillData();
		FillData();	
	
	}
}
