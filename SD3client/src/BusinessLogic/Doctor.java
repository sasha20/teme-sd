package BusinessLogic;

import java.io.Serializable;
import java.util.Set;

public class Doctor implements Serializable {

	private int iddoctor;
	private String docName;
	private String specializare;
	private String availability;
	private Set cons;
	
	
	public Set getCons() {
		return cons;
	}
	public void setCons(Set cons) {
		this.cons = cons;
	}
	public Doctor(){}
	public Doctor(String docName, String specializare, String availability){
		this.docName =docName;
		this.specializare = specializare;
		this.availability = availability;
	}
	public int getIddoctor() {
		return iddoctor;
	}
	public void setIddoctor(int iddoctor) {
		this.iddoctor = iddoctor;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getSpecializare() {
		return specializare;
	}
	public void setSpecializare(String specializare) {
		this.specializare = specializare;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	
	

}
