package Main;

import javax.swing.UIManager;

import Presentation.LogInView;

public class Main {

 
	public static void main(String arg[]) {
	
		try{
			LogInView li= new LogInView();
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			li.setVisible(true);
		}catch(Exception e){e.printStackTrace();}
	}
}

 