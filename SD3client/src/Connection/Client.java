package Connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.table.DefaultTableModel;

import BusinessLogic.Consultation;
import BusinessLogic.Doctor;
import BusinessLogic.Patient;
import BusinessLogic.User;

public class Client {

	public static final int PORT=9990;
	public static final String HOSTNAME="localhost";
	static Socket socket=null;

	public static void connect() throws UnknownHostException, IOException {
		System.out.println("Attempting to connect to " + HOSTNAME + ":" + PORT);
		socket = new Socket(HOSTNAME, PORT);
		System.out.println("Connection Established");
	}

	public static void passObjectToServer(Object o ){
		try {
			ObjectOutputStream  oos= new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o); 
			System.out.println("Obiectul a fost trimis catre server" + o.toString());
			//socket.close();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	
	//RECIEVE USER
	public static Object receiveObjectFromServer(){
		Object obj=new Object();
		 
		try {
			ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
			obj=ois.readObject(); 
			//System.out.println(ois);
	 		User u=(User)obj;
			System.out.println("Obiectul a fost primit de catre client" +u.getId());
		//	socket.close(); 
		} catch (IOException e) { 
			e.printStackTrace();
		} catch (ClassNotFoundException e) { 
			e.printStackTrace();
		}
		return obj;
	}
	
	//RECIEVE DOCTOR
	public static Object receiveDoctorFromServer(){
		Object obj=new Object();
		 
		try {
			ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
			obj=ois.readObject(); 
			//System.out.println(ois);
	 		Doctor u=(Doctor)obj;
			System.out.println("Obiectul a fost primit de catre client" +u.getIddoctor());
		//	socket.close(); 
		} catch (IOException e) { 
			e.printStackTrace();
		} catch (ClassNotFoundException e) { 
			e.printStackTrace();
		}
		return obj;
	}
	
	
	//RECIEVE PATIENT
	public static Object receivePatientFromServer(){
		Object obj=new Object();
		 
		try {
			ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
			obj=ois.readObject(); 
			//System.out.println(ois);
	 		Patient u=(Patient)obj;
			System.out.println("Pacientul a fost primit de catre client" +u.getId());
		//	socket.close(); 
		} catch (IOException e) { 
			e.printStackTrace();
		} catch (ClassNotFoundException e) { 
			e.printStackTrace();
		}
		return obj;
	}
	
	
	//RECIEVE Consultation
		public static Object receiveConsultationFromServer(){
			Object obj=new Object();
			 
			try {
				ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
				obj=ois.readObject(); 
				//System.out.println(ois);
		 		Consultation u=(Consultation)obj;
				//System.out.println("Consultatia a fost primita de catre client" +u.getDetails());
			//	socket.close(); 
			} catch (IOException e) { 
				e.printStackTrace();
			} catch (ClassNotFoundException e) { 
				e.printStackTrace();
			}
			return obj;
		}
	
	
	//CONS ID
	public static void passIdToServer(int o ){
		try {
			ObjectOutputStream  oos= new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o); 
		//	System.out.println("Ordinul a fost trimis la server" + o.toString());
			//socket.close();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	
	//MESSAGES
	public static void passMessageToServer(String o ){
		try {
			ObjectOutputStream  oos= new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o); 
			System.out.println("Ordinul a fost trimis la server" + o.toString());
			//socket.close();
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	public static String receiveMessageFromServer(){
		String fmm = null;
		 
		try {
			ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
			fmm=(String)ois.readObject();  
			System.out.println("Ordinul:" + fmm+  " a fost primit de catre client");
		  //  socket.close();
			return fmm;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fmm;
		
	}
	
	
	 
	
	public static void passTableToServer(DefaultTableModel o ){
		try {
			ObjectOutputStream  oos= new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o); 
			System.out.println("Tabela a fost trimisa catre server"); 
			//socket.close();
		} catch (IOException e) {
		 
			e.printStackTrace();
		}
	}
	



	public static Object receiveTableFromServer(){
		Object obj=new Object();
		try {
			ObjectInputStream  ois= new ObjectInputStream(socket.getInputStream());
			obj=ois.readObject(); 
			System.out.println(ois); 
			DefaultTableModel u=(DefaultTableModel)obj;
		  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			 
			e.printStackTrace();
		}
		return obj;
	}


 
	

	}
